package com.example.amadzon2

import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.activity.addCallback
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_item_details.*
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class ItemDetailsFragment : Fragment() , OnMapReadyCallback{

    private lateinit var navController: NavController
    private lateinit var googleMap: GoogleMap
    private var product_id: Int = -1
    private var isMine : Boolean = false
    private var isModified: Boolean = false
    private var amIInterested: Boolean = false
    private var rated :Boolean = false
    private lateinit var loc: String
    private var state :Int = -1
    private var owner: String =""
    private lateinit var vm : ItemViewModel
    private lateinit var vmu : UserViewModel
    private val FCM_API = "https://fcm.googleapis.com/fcm/send"
    private val serverkey =
        "key=" + "AAAAcaJU9K8:APA91bGR_1DhWaQzLX75hUpdpjlqORLDk4mEeM7DviW9ZjOxetMv0brePRKFjOXpFUjjrzdyPKfKpBTfXAMhDRPanekQUVcrKOhZDb18SjYZMuCqJRa_vZ1iTMhlLjWJRlt7vIELtStB"
    private val contentType = "application/json"
    private val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        vm = ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)
        vmu = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)
        isMine = requireArguments().getBoolean("group06.lab3.MYITEMS")
        isModified = requireArguments().getBoolean("group06.lab3.MODIFIED")
        state = requireArguments().getInt("group06.lab3.STATE")
        rated = requireArguments().getBoolean("group06.lab3.RATED")

        val isNotification = requireArguments().getBoolean("group06.lab3.NOTIFICATION")
        val x = requireArguments().getString("group06.lab3.PRODUCT_ID")
        product_id = x?.toInt()!!
        owner = if(!isMine) requireArguments().getString("group06.lab3.OWNER").toString() else vm.getUser()

        if(isNotification) {
            vm.initItemLD(product_id, owner)
            vmu.initDB(vmu.getCurrentUserID(), { (activity as MainActivity).updateDrawer() }, {})
        }

        if(isModified) {
            requireActivity().onBackPressedDispatcher.addCallback(this) {
                findNavController().navigate(R.id.itemListFragment, null, getNavOptionsBack())
            }
        }
    }

    private fun displayLocation(pos: String){
        val geocoder = Geocoder(requireContext(), Locale.ENGLISH)
        var addresses: List<Address> = emptyList()
        try {
            addresses = geocoder.getFromLocationName(pos, 1)
        }
        catch (e: IOException) {
            Toast.makeText(requireContext(), "Location loading failed", Toast.LENGTH_LONG).show()
        }

        if(addresses.isNotEmpty()) {
            val longitude1 = addresses[0].longitude
            val latitude1= addresses[0].latitude
            val itemL = LatLng(latitude1, longitude1)

            googleMap.addMarker(
                MarkerOptions().position(itemL)
                    .title("Item Location")
                    .snippet("The Item is in $loc")
            )
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(itemL, 12.0f))
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        itemdetails_map_view.onCreate(savedInstanceState)
        itemdetails_map_view.onResume()
        itemdetails_map_view.getMapAsync(this)
    }

    override fun onMapReady(map: GoogleMap?) {
        map?.let {
            googleMap = it
            //googleMap.uiSettings.isMapToolbarEnabled = false
            if(!isMine) {
                googleMap.setOnMapClickListener {
                    val bundle = bundleOf(
                        "group06.lab3.ITEMLOC" to loc
                    )
                    navController.navigate(
                        R.id.action_itemDetailsFragment_to_fullMapFragment,
                        bundle,
                        getNavOptionsForward()
                    )
                }
            }
            if(this::loc.isInitialized)
                displayLocation(refactorLocation(loc))
        }
    }

    private fun refactorLocation(l: String) : String{
        val tmp = l.replace(" ", "%20")
        return tmp.replace("+", "%2B")
    }

    private fun getNavOptionsBack(): NavOptions? {
        return NavOptions.Builder()
            .setEnterAnim(R.anim.slide_in_left)
            .setExitAnim(R.anim.slide_out_right)
            .setPopEnterAnim(R.anim.slide_in_right)
            .setPopExitAnim(R.anim.slide_out_left)
            .setPopUpTo(R.id.mobile_navigation, true)
            .build()
    }

    private fun getNavOptionsForward(): NavOptions? {
        return NavOptions.Builder()
            .setEnterAnim(R.anim.slide_in_right)
            .setExitAnim(R.anim.slide_out_left)
            .setPopEnterAnim(R.anim.slide_in_left)
            .setPopExitAnim(R.anim.slide_out_right)
            .build()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_item_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        vm.getItemLD().observe( viewLifecycleOwner, Observer { item ->
            if (item != null) {
                short_description.text = item.name
                price.text = NumberFormat.getCurrencyInstance(Locale.ITALY).format(item.price)
                full_description.text = item.description
                category.text = item.category
                loc = item.location
                if(this::googleMap.isInitialized)
                    displayLocation(refactorLocation(item.location))
                val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
                val d = sdf.format(item.date.toDate())
                expiration_date.text = d
                if (item.localImagePath != "")
                    product_image.setImageURI(Uri.fromFile(File(item.localImagePath)))
                if(this::googleMap.isInitialized)
                    displayLocation(refactorLocation(item.location))
                rated = item.rated
            }
        })

        if(!isMine && state == VALID) {
            vmu.checkSubscriptionToItem(product_id, owner, { isObserved() }, { isNotObserved() }, {
                Log.w("aMADzon", "Could not check the subscription")
            })

            fab_observe.setOnClickListener {
                if(!amIInterested){
                    vmu.subscribeToItem(product_id, owner, vmu.getCurrentUserID(),
                        { Snackbar.make(view, "You are now interested to this item", Snackbar.LENGTH_SHORT).show(); isObserved()
                            sendNotification(owner, product_id)},
                        { Snackbar.make(view, "Ops! Something went wrong", Snackbar.LENGTH_SHORT).show() })


                } else {
                    vmu.unsuscribeToItem(product_id, owner, vmu.getCurrentUserID(),
                        { Snackbar.make(view, "Item removed from interests list", Snackbar.LENGTH_SHORT).show(); isNotObserved() },
                        { Snackbar.make(view, "Ops! Something went wrong", Snackbar.LENGTH_SHORT).show() })
                }
            }
            fab_observe.visibility = View.VISIBLE
        }
        else {
            fab_observe.visibility = View.GONE
            vmu.resetList()
        }

        setHasOptionsMenu((isMine && state==VALID) || (!isMine && state == SOLD && !rated))
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if(isMine && state == VALID) inflater.inflate(R.menu.my_item_menu, menu)
        else if (!isMine && state == SOLD && !rated) inflater.inflate(R.menu.evaluate_menu,menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                if(isModified)
                    findNavController().navigate(R.id.itemListFragment, null, getNavOptionsBack())
                true
            }
            R.id.pencil_menu -> {
                val bundle = bundleOf(
                    "group06.lab3.NEW_PRODUCT_FLAG" to false,
                    "group06.lab3.PRODUCT_ID" to product_id,
                    "group06.lab3.FROM_DETAILS" to true,
                    "group06.lab3.OWNER" to owner
                )
                navController.navigate(
                    R.id.action_itemDetailsFragment_to_itemEditFragment,
                    bundle,
                    getNavOptionsForward()
                )
                true
            }
            R.id.interested_users_menu -> {
                val bundle = bundleOf("group06.lab3.PRODUCT_ID" to product_id.toString(),
                    "group06.lab3.OWNER" to owner)
                vmu.initListIds(product_id, owner
                    , {Log.w("aMADzon", "Error")})
                navController.navigate(
                    R.id.action_itemDetailsFragment_to_interestedUsersListFragment, bundle
                )
                true
            }
            R.id.delete_menu -> {
                vmu.initListIds(product_id, owner
                    , {Log.w("aMADzon", "Error")})
                notifyRemoval()
                vm.invalidateItem(owner,product_id)
                {Snackbar.make(requireView(), "This item can not be sold anymore", Snackbar.LENGTH_SHORT).show()
                    findNavController().navigate(R.id.itemListFragment, null, getNavOptionsBack())}

                true
            }
            R.id.evaluate_menu -> {
                navController.navigate(R.id.action_itemDetailsFragment_to_ratingFragment)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt("group06.lab3.PRODUCT_ID", product_id)
        super.onSaveInstanceState(outState)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState != null) {
            product_id = savedInstanceState.getInt("group06.lab3.PRODUCT_ID")
        }
    }

    private fun isObserved(){
        amIInterested = true
        fab_observe.background.setTint(Color.parseColor("#C0392B"))
        fab_observe.setImageResource(R.drawable.ic_fire_block)
    }

    private fun isNotObserved(){
        amIInterested = false
        fab_observe.background.setTint(ContextCompat.getColor(requireContext(), R.color.colorAccent))
        fab_observe.setImageResource(R.drawable.ic_fire)
    }

    private fun sendNotification(idUser: String, idItem: Int) {
        val topic = "/topics/topic_"+idUser
        val notification = JSONObject()
        val notificationBody = JSONObject()

        try{
            notificationBody.put("type", "I")
            notificationBody.put("title", "aMADzon")
            notificationBody.put("message", "Someone is interested in one of your items!")
            notificationBody.put("idItem", idItem)
            notificationBody.put("idUser", idUser)
            notificationBody.put("destID", idUser)

            notification.put("to", topic)
            notification.put("data", notificationBody)
        } catch (e: JSONException) {
            Log.e("aMADzon", e.message.toString())
        }

        val jsonObjectRequest = object : JsonObjectRequest(FCM_API, notification,
            Response.Listener<JSONObject> { response ->
                Log.i("aMADzon", "onResponse: $response")
            },
            Response.ErrorListener {
                Toast.makeText(context, "Request Error", Toast.LENGTH_LONG).show()
            }) {
            override fun getHeaders(): MutableMap<String, String> {
                val params = HashMap<String, String>()
                params["Authorization"] = serverkey
                params["Content-Type"] = contentType
                return params
            }
        }
        requestQueue.add(jsonObjectRequest)
    }

    private fun notifyRemoval() {
        val notification = JSONObject()
        val notificationBody = JSONObject()
        val validationMap = HashMap<String, Boolean> ()

        vmu.getList().observe(viewLifecycleOwner, Observer {
            for(iu in it){
                /*questa linea ha subìto il refactor: significa che se l'utente non c'era ancora
                * validationMap[iu.id] == null -> è true, quindi da notificare
                * altrimenti validationMap[iu.id] == null -> è false, già notificato
                * */
                validationMap[iu.id] = validationMap[iu.id] == null
            }

            for (iu in it){
                if(validationMap[iu.id]!!){
                    val topic = "/topics/topic_" + iu.id
                    try {
                        notificationBody.put("type", "R")
                        notificationBody.put("title", "aMADzon")
                        notificationBody.put("message", "An item you showed interest to is no longer on sale!")
                        notificationBody.put("destID", iu.id)
                        notification.put("to", topic)
                        notification.put("data", notificationBody)
                    } catch (e: JSONException) {
                        Log.e("aMADzon", e.message.toString())
                    }

                    val jsonObjectRequest = object : JsonObjectRequest(FCM_API, notification,
                        Response.Listener<JSONObject> { response ->
                            Log.i("aMADzon", "onResponse: $response")
                        },
                        Response.ErrorListener {
                            Toast.makeText(context, "Request Error", Toast.LENGTH_LONG).show()
                        }) {
                        override fun getHeaders(): MutableMap<String, String> {
                            val params = HashMap<String, String>()
                            params["Authorization"] = serverkey
                            params["Content-Type"] = contentType
                            return params
                        }
                    }
                    requestQueue.add(jsonObjectRequest)
                }
            }
        })
    }
}