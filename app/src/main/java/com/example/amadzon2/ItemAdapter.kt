package com.example.amadzon2

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.io.File
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

class ItemAdapter(var items: ArrayList<Item>, isMine: Boolean): RecyclerView.Adapter<ItemAdapter.ItemViewHolder>(),
    Filterable {

    var onItemClick: ((Item) -> Unit)? = null
    var onButtonClick: ((Item) -> Unit)? = null
    var mine = isMine
    var itemListFull : ArrayList<Item> = ArrayList(items)

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item, parent, false)
        return ItemViewHolder(v, mine)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(charSequence: CharSequence?, filterResults: FilterResults) {
                @Suppress("UNCHECKED_CAST")
                items = filterResults.values as ArrayList<Item>
                notifyDataSetChanged()
            }

            override fun performFiltering(charSequence: CharSequence?): FilterResults {
                val queryString = charSequence?.toString()?.toLowerCase(Locale.ROOT)

                val filterList = ArrayList<Item>()
                val filterResults = FilterResults()

                if (queryString==null || queryString.isEmpty() || queryString == "")
                    filterList.addAll(itemListFull)
                else {
                    filterList.addAll(itemListFull.filter {
                        it.name.toLowerCase(Locale.ROOT).contains(queryString) ||
                        it.location.toLowerCase(Locale.ROOT).contains(queryString) ||
                        it.description.toLowerCase(Locale.ROOT).contains(queryString) ||
                        it.category.toLowerCase(Locale.ROOT).contains(queryString)
                    })
                }
                filterResults.values = filterList
                filterResults.count = filterList.count()
                return filterResults
            }
        }
    }

    inner class ItemViewHolder(v: View, isMine: Boolean): RecyclerView.ViewHolder(v) {
        private val name: TextView = v.findViewById(R.id.item_name)
        private val price: TextView = v.findViewById(R.id.item_price)
        private var format: NumberFormat = NumberFormat.getCurrencyInstance(Locale.ITALY)
        private var editButton: ImageView = v.findViewById(R.id.edit_button)
        private val img: ImageView = v.findViewById(R.id.item_image)
        private val loc: TextView = v.findViewById(R.id.item_location)
        private val alert: ImageView = v.findViewById(R.id.alert)
        private val sold: ImageView = v.findViewById(R.id.sold)
        private val mine : Boolean = isMine

        init {
            v.setOnClickListener {
                onItemClick?.invoke(items[adapterPosition])
            }
            if(mine){
                editButton.setOnClickListener {
                    onButtonClick?.invoke(items[adapterPosition])
                }
            } else {
                editButton.visibility = View.GONE
            }
        }

        fun bind(i:Item) {
            when (i.state) {
                VALID -> {
                    if (mine) editButton.visibility = View.VISIBLE
                    else editButton.visibility = View.GONE
                    alert.visibility = View.GONE
                    sold.visibility = View.GONE
                }
                SOLD -> {
                    if (mine) sold.visibility = View.VISIBLE
                    else sold.visibility = View.GONE
                    alert.visibility = View.GONE
                    editButton.visibility = View.GONE
                }
                LOCKED -> {
                    if (mine) alert.visibility = View.VISIBLE
                    else alert.visibility = View.GONE
                    sold.visibility = View.GONE
                    editButton.visibility = View.GONE
                }
            }
            name.text = i.name
            price.text = format.format(i.price)
            loc.text = i.location
            if (i.localImagePath == "")
                img.setImageResource(R.drawable.product)
            else img.setImageURI(Uri.fromFile(File(i.localImagePath)))
        }
    }
}