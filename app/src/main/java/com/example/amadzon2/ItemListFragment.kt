package com.example.amadzon2

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_item_list.*
import java.io.FileNotFoundException


class ItemListFragment : Fragment() {
    private var navController: NavController? = null
    private var adapter: ItemAdapter? = null
    private lateinit var itemList : ArrayList<Item>
    private lateinit var vm : ItemViewModel
    private val myItems : Boolean  = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootview = inflater.inflate(R.layout.fragment_item_list, container, false)

        //loadItems()
        vm = ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)
        vm.invalidateMyItems()

        itemList = ArrayList()


        (rootview.findViewById(R.id.itemList) as RecyclerView).also {
            adapter = setupAdapter(it, itemList)
        }

        return rootview
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        vm.getItemsOfTheUser().observe(viewLifecycleOwner, Observer{
            items ->
            progressbar_il.hide()
            itemList.removeAll(itemList)
            itemList.addAll(items)
            adapter!!.notifyDataSetChanged()
            UserRepository.triggerId(vm.getUser())
            if(items.size != 0)
                hideMessage()
            else
                displayMessage()
        })


        adapter!!.onItemClick = { item ->
            val bundle = bundleOf(
                "group06.lab3.PRODUCT_ID" to item.id.toString(),
                "group06.lab3.MYITEMS" to myItems,
                "group06.lab3.OWNER" to item.user,
                "group06.lab3.MODIFIED" to false,
                "group06.lab3.STATE" to item.state
            )
            vm.initItemLD(item)
            navController!!.navigate(R.id.action_itemListFragment_to_itemDetailsFragment, bundle)
        }
        adapter!!.onButtonClick = { item ->
            if(item.state == VALID) {
                val bundle = bundleOf(
                    "group06.lab3.NEW_PRODUCT_FLAG" to false,
                    "group06.lab3.PRODUCT_ID" to item.id,
                    "group06.lab3.OWNER" to vm.getUser()
                )
                vm.initItemLD(item)
                navController!!.navigate(R.id.action_itemListFragment_to_itemEditFragment, bundle)
            }
        }

        fab.setOnClickListener {
            val bundle = bundleOf(
                "group06.lab3.NEW_PRODUCT_FLAG" to true,
                "group06.lab3.FROM_DETAILS" to false
            )
            vm.initItemLD()
            vm.newImagePath = ""
            navController!!.navigate(R.id.action_itemListFragment_to_itemEditFragment, bundle)
        }
    }

    override fun onStart() {
        super.onStart()
        val a = requireActivity() as MainActivity
        UserRepository.getIdProduct().observe(this, Observer {
            a.setItemId(it) })
        UserRepository.triggerId(vm.getUser())
    }

    private fun setupAdapter(v: RecyclerView, a: ArrayList<Item>) : ItemAdapter {
        v.layoutManager = LinearLayoutManager(activity)
        val ad = ItemAdapter(a, true)
        v.adapter = ad
        return ad
    }

    private fun isValidUri(uri: String): Boolean {
        return try {
            requireActivity().contentResolver.openInputStream(Uri.parse(uri))
            true
        } catch (e: FileNotFoundException) {
            false
        }
    }

    private fun hideMessage() {
        emptyList.visibility = View.GONE
    }

    private fun displayMessage() {
        emptyList.visibility = View.VISIBLE
    }
}