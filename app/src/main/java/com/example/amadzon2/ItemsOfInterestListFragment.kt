package com.example.amadzon2

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_items_of_interest_list.*


class ItemsOfInterestListFragment : Fragment() {

    private var navController: NavController? = null
    private var adapter: ItemAdapter? = null
    private lateinit var itemList : ArrayList<Item>
    private val myItems : Boolean  = false
    private lateinit var vm : UserViewModel
    private lateinit var vmi :ItemViewModel
    private lateinit var thisUser : String
    private lateinit var id : String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)
        vmi = ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)
        id = vm.getCurrentUserID()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_items_of_interest_list, container, false)

        itemList = ArrayList()

        thisUser = vm.getCurrentUserID()
        vm.initItemIds(thisUser)
        { Log.w("aMADzon", "Error in initializing the interested item lists")}

        (rootView.findViewById(R.id.interestedItemList) as RecyclerView).also {
            adapter = setupAdapter(it, itemList)
        }

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        setHasOptionsMenu(true)

        vm.invalidateInterestedItems()

        vm.getInterestedItemList().observe(viewLifecycleOwner, Observer{ items ->
            progressbar_ii.hide()
            itemList.removeAll(itemList)
            itemList.addAll(items)
            adapter!!.notifyDataSetChanged()
            if(items.size != 0) {
                hide(view)
            }
            else {
                display(view)
            }
        })

        adapter!!.onItemClick = { item ->
            val bundle = bundleOf(
                "group06.lab3.PRODUCT_ID" to item.id.toString(),
                "group06.lab3.MYITEMS" to myItems,
                "group06.lab3.OWNER" to item.user,
                "group06.lab3.MODIFIED" to false,
                "group06.lab3.STATE" to item.state
            )
            vmi.initItemLD(item)
            navController!!.navigate(R.id.action_itemsOfInterestListFragment_to_itemDetailsFragment, bundle)
        }

        /*if(adapter!!.itemCount != 0) {
            hide(view)
        } else {
            display(view)
        }*/
    }

    private fun setupAdapter(v: RecyclerView, a: ArrayList<Item>) : ItemAdapter {
        v.layoutManager = LinearLayoutManager(activity)
        val ad = ItemAdapter(a, false)
        v.adapter = ad
        return ad
    }

    private fun hide(view: View){
        (view.findViewById(R.id.emptyInterestedItemListMessage) as TextView).also {
            it.visibility = View.GONE
        }
    }

    private fun display(view: View){
        (view.findViewById(R.id.emptyInterestedItemListMessage) as TextView).also {
            it.visibility = View.VISIBLE
        }
    }
}
