package com.example.amadzon2

import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.show_profile.*
import java.io.File
import java.io.IOException
import java.util.*


class ShowProfileFragment : Fragment(), OnMapReadyCallback {
    lateinit var navController : NavController
    private lateinit var googleMap: GoogleMap
    private lateinit var loc: String
    private var orientation: Int = 0
    var otherUser = false

    lateinit var vm :UserViewModel
    lateinit var storage :StorageReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)
        storage = FirebaseStorage.getInstance().reference
        if (arguments != null)
            otherUser = requireArguments().getBoolean("group06.lab3.OTHER_USER")
    }

    // From ShowProfile I always want to navigate to home when back button is pressed
    private fun getNavOptions(): NavOptions? {
        return NavOptions.Builder()
            .setEnterAnim(R.anim.fade_in)
            .setExitAnim(R.anim.fade_out)
            .setPopEnterAnim(R.anim.fade_in)
            .setPopExitAnim(R.anim.fade_out)
            .setPopUpTo(R.id.mobile_navigation, true) // In this way, if user after navigation press back button, this fragment will not be in the stack anymore
            .build()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        showprofile_map_view.onCreate(savedInstanceState)
        showprofile_map_view.onResume()
        showprofile_map_view.getMapAsync(this)
    }

    override fun onMapReady(map: GoogleMap?) {
        map?.let {
            googleMap = it

            if(this::loc.isInitialized)
                displayLocation(refactorLocation(loc))
        }
    }

    private fun refactorLocation(l: String) : String{
        val tmp = l.replace(" ", "%20")
        return tmp.replace("+", "%2B")
    }

    private fun displayLocation(pos: String){
        val geocoder = Geocoder(requireContext(), Locale.ENGLISH)
        var addresses: List<Address> = emptyList()
        try {
            addresses = geocoder.getFromLocationName(pos, 1)
        }
        catch (e: IOException) {
            Toast.makeText(requireContext(), "Location loading failed", Toast.LENGTH_LONG).show()
        }

        if(addresses.isNotEmpty()) {
            val longitude1 = addresses[0].longitude
            val latitude1= addresses[0].latitude
            val itemL = LatLng(latitude1, longitude1)

            googleMap.addMarker(
                MarkerOptions().position(itemL)
                    .title("Your Location")
                    .snippet("This is your location")
            )
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(itemL, 12.0f))
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        orientation = resources.configuration.orientation
        return inflater.inflate(R.layout.show_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        profile_image.setImageResource(R.drawable.user)
        if (otherUser) {
            vm.getIntUser().observe(viewLifecycleOwner, Observer<InterestedUser> {
                show_nickname.text = it.nickname
                show_email.text = it.email
                if (it.localImagePath != "")
                    profile_image.setImageURI(Uri.fromFile(File(it.localImagePath)))
                if(it.nratings != 0) {
                    var nratings = "${it.nratings} seller ratings"
                    if (it.nratings == 1)
                        nratings = "1 seller rating"
                    num_ratings.text = nratings
                    val rating = it.rating/it.nratings
                    rating_bar.rating = rating
                    rating_avg.text = rating.toString()
                }
                else rating_avg.visibility = View.GONE
            })
            show_fullName.visibility = View.INVISIBLE
            showprofile_map_view.visibility = View.GONE
            ratings_button.visibility = View.GONE
            setHasOptionsMenu(false)
            (activity as MainActivity).setArrowInToolbar()
        }
        else {
            vm.getUser().observe(viewLifecycleOwner, Observer<User> { res ->
                show_fullName.text = res.full_name
                show_email.text = res.email
                show_nickname.text = res.nickname
                loc = res.location
                if(this::googleMap.isInitialized)
                        displayLocation(refactorLocation(loc))
                if (res.localImagePath != "")
                    profile_image.setImageURI(Uri.fromFile(File(res.localImagePath)))
                (activity as MainActivity).updateDrawer()
                if(res.nratings != 0) {
                    var nratings = "${res.nratings} ratings"
                    if (res.nratings == 1)
                        nratings = " 1 rating"
                    num_ratings.text = nratings
                    val rating = res.rating/res.nratings
                    rating_bar.rating = rating
                    rating_avg.text = rating.toString()
                    ratings_button.setOnClickListener {
                        vm.initEvalList(res.id)
                        val bundle = bundleOf("group06.lab4.NOTIFICATION" to false)
                        navController.navigate(R.id.action_showProfileFragment_to_evaluationsListFragment, bundle)
                    }
                }
                else {
                    ratings_button.visibility = View.GONE
                    rating_avg.visibility = View.GONE
                }
            })
            setHasOptionsMenu(true)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean("group06.lab3.OTHER_USER", otherUser)
        super.onSaveInstanceState(outState)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState != null) {
            otherUser = savedInstanceState.getBoolean("group06.lab3.OTHER_USER")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.pencil_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.pencil_menu -> {
                navController.navigate(R.id.action_showProfileFragment_to_editProfileFragment)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
