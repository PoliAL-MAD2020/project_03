package com.example.amadzon2


import android.os.Bundle
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import java.io.File

class UserViewModel : ViewModel() {

    private val mStorageRef: StorageReference = FirebaseStorage.getInstance().reference

    private var n = ""
    private var e = ""
    private var mUser = MutableLiveData<User> ()
    private var buyItemList = MutableLiveData<ArrayList<Item>> ()

    private var mIntUsers = MutableLiveData<ArrayList<InterestedUser>> ()
    private var mSelectedUser = MutableLiveData<InterestedUser> ()
    private val idList = ArrayList<String> ()

    private var interestedItems = MutableLiveData<ArrayList<Item>> ()
    private val idInterestedItem = ArrayList<Pair<Int, String>> ()

    private var ratingList = MutableLiveData<ArrayList<Evaluation>> ()


    fun getCurrentUserID() : String {
        return FirebaseAuth.getInstance().currentUser?.uid.toString()
    }

    fun invalidateInterestedItems() {
        interestedItems = MutableLiveData<ArrayList<Item>>()
        ItemRepository.clearInterestedItem()
    }

    //Actual User

    fun getUser(): MutableLiveData<User> {
        return mUser
    }

    fun getBuyList(): MutableLiveData<ArrayList<Item>> {
        return buyItemList
    }

    fun initUser(uid: String, name: String, email: String,
                 login: ()-> Unit, register : () -> Unit, error: () -> Unit){
        n = name
        e = email
        UserRepository.checkUser(uid, login, register, error, ::initDB)
    }

    fun finalInit(uid: String, nickname: String, location: String, lastId: Int,  ok: ()-> Unit, error: () -> Unit ) {
        val user = User(n, e, nickname, location, uid, lastId, 0.0f, 0)
        UserRepository.setUser(uid, user, ok, error, ::getUserData)
    }

    fun setUser(uid: String, name: String, email: String, nickname: String, location: String, lastId :Int,
                ok: ()-> Unit, error: () -> Unit ) {
        val user = User(name, email, nickname, location, uid, lastId, 0.0f, 0)
        UserRepository.updateUser(uid, user, ok, error, ::getUserData)
    }

    fun initDB(uid :String, ok :() -> Unit, error :() -> Unit) {
        UserRepository.getUser(uid, ::getUserData, ok, error)
        //initBuyList(uid, error)
    }

    fun initBuyList(uid :String, error :() -> Unit ) {
        buyItemList = MutableLiveData<ArrayList<Item>> ()
        ItemRepository.getItems().addSnapshotListener { snapshot, e ->
            if (e != null)
                error()
            if (snapshot != null) {
                val items = ArrayList<Item>()
                val doc = snapshot.documents
                buyItemList.value = items
                doc.forEach {
                    val i = it.toObject(Item::class.java)
                    if (i != null) {
                        if (i.state == SOLD && i.buyerID == uid) {
                            if (i.localImagePath == "") {
                                items.add(i)
                                buyItemList.value = items
                            } else {
                                val imageRef = mStorageRef.child("item/${i.user}/${i.id}.jpg")
                                val localFile = File.createTempFile("${i.user}::${i.id}", ".jpg")
                                imageRef.getFile(localFile)
                                    .addOnSuccessListener {
                                        i.localImagePath = localFile.absolutePath
                                        items.add(i)
                                        buyItemList.value = items
                                    }
                                    .addOnFailureListener {
                                        localFile.delete()
                                        items.add(i)
                                        buyItemList.value = items
                                    }
                            }
                        }
                    }
                }
            }
        }
    }

    fun initEvalList(uid :String) {
        ratingList = MutableLiveData<ArrayList<Evaluation>> ()
        UserRepository.getRatings(uid).addSnapshotListener { snapshot, e ->
            if (e != null)
                Log.w("aMADzon", "some error happened in init")
            if (snapshot != null) {
                val ratings = ArrayList<Evaluation>()
                val doc = snapshot.documents
                doc.forEach {
                    val i = it.toObject(Evaluation::class.java)
                    if (i != null) {
                        ratings.add(i)
                        ratingList.value = ratings
                    }
                }
            }
        }
    }

    fun getEvalList(): MutableLiveData<ArrayList<Evaluation>> {
        return ratingList
    }

    private fun getUserData () {
        mUser = UserRepository.getUserData()
    }

    fun updateImage(path: String, ok: () -> Unit, error: () -> Unit) {
        UserRepository.updateImage(getCurrentUserID(), path, ok, error)
    }

    fun successfulUpdateImage(ok: () -> Unit, error: () -> Unit){
        UserRepository.getImage(getCurrentUserID(), ::getUserData, ok, error)
    }

    fun setNewPath(path: String) {
        mUser.value!!.newImagePath = path
    }

    fun reset() {
        mUser = MutableLiveData<User> ()
        UserRepository.reset()
    }

    //Interested Users

    fun initListIds(iid: Int, uid: String, error: () -> Unit) {
        InterestedUsersRepository.getIdList(iid, uid, ::initListUsers, error, ::fromIdToUser)
    }

    fun initItemIds(userId: String, error: () -> Unit){
        UserRepository.getPairList(userId, ::initListItems, error, ::fromIdToItem)
    }

    fun clearInterestedUsers(){
        InterestedUsersRepository.clearInterestedCollection()
    }

    private fun fromIdToUser(){
        if(idList.isEmpty()){
            initList()
        }else {
            for (id in idList) {
                InterestedUsersRepository.getUser(
                    id,
                    { initList() },
                    { Log.w("aMADzon", "I tried to load details of $id") })
            }
        }
    }

    private fun fromIdToItem(){
        if(idInterestedItem.isEmpty()){
            getListItems()
        }else {
            for (p in idInterestedItem) {
                ItemRepository.getAnItem( p.second, p.first, { getListItems() },
                    { Log.w("aMADzon", "I tried to load the item ${p.first} of the ${p.second}") })
            }
        }
    }

    fun getUserLocation() : LiveData<String>{
        val loc = MutableLiveData<String> ()
        UserRepository.getLocation(getCurrentUserID(),
            { location -> loc.value = location},
            {Log.w("aMADzon", "Unable to get the user location")})
        return loc
    }

    private fun initListUsers(id : String, ok :() -> Unit, error: () -> Unit) {
        if(id == ""){
            idList.clear()
            ok()
        } else {
            InterestedUsersRepository.getUserList(id).get().addOnSuccessListener {
                val l = ArrayList<String>()
                for (document in it.documents) {
                    val k = document.id.split("/")
                    val n = k.size
                    l.add(k[n - 1])
                }
                idList.clear()
                idList.addAll(l)
                ok()
            }.addOnFailureListener {
                error()
            }
        }
    }

    private fun initListItems(id : String, ok :() -> Unit, error: () -> Unit) {
        if(id == ""){
            idInterestedItem.clear()
            ok()
        } else {
            UserRepository.getInterestedList(id).get().addOnSuccessListener {
                val l = ArrayList<Pair<Int, String>>()
                for (document in it.documents) {
                    val m = document.data
                    if(m != null){
                        val k = Pair<Int, String>(m["id"].toString().toInt(),m["owner"].toString())
                        l.add(k)
                    }
                }
                idInterestedItem.clear()
                idInterestedItem.addAll(l)
                ok()
            }.addOnFailureListener {
                error()
            }
        }
    }

    private fun getListItems(){
        interestedItems.value = ItemRepository.getIIList()
    }

    fun getInterestedItemList() : MutableLiveData<ArrayList<Item>>{
        return interestedItems
    }

    private fun initList() {
        mIntUsers.value = InterestedUsersRepository.getList().value
    }

    fun getList() : LiveData<ArrayList<InterestedUser>> {
        return mIntUsers
    }

    fun initUser(uid :String, ok :(bundle :Bundle) -> Unit, bundle :Bundle, error :() -> Unit) {
        val list = mIntUsers.value
        var user :InterestedUser? = null
        if (list != null) {
            for (element in list) {
                if (element.id == uid){
                    user = element
                    break
                }
            }
        }
        if(user != null) {
            mSelectedUser = MutableLiveData<InterestedUser> (user)
            ok(bundle)
        }
        else error()
    }

    fun getIntUser(): MutableLiveData<InterestedUser> {
        return mSelectedUser
    }
    
    fun resetList() {
        mIntUsers = MutableLiveData<ArrayList<InterestedUser>>()
    }


    fun subscribeToItem(iid:Int, owner: String,  other_user: String, ok: () -> Unit, error: () -> Unit) {
        InterestedUsersRepository.addIntUser(iid, owner, other_user, ok, error)
        UserRepository.insertInterestedItem(iid, owner, other_user, {Log.w("IITEM", "Everything ok, insertion done")},
            {Log.w("IITEM", "some error happened in insertion")})
    }

    fun unsuscribeToItem(iid:Int, owner: String, other_user: String, ok: () -> Unit, error: () -> Unit) {
        InterestedUsersRepository.removeIntUser(iid, owner, other_user, ok, error)
        UserRepository.deleteInterestedItem(iid, owner, other_user, {Log.w("aMADzon", "Everything ok, deletion done")},
            {Log.w("aMADzon", "some error happened in deletion")})
    }

    fun checkSubscriptionToItem(iid:Int, uid: String, subscribeAction: () -> Unit, unsubscribeAction: () -> Unit, error: () -> Unit) {
        InterestedUsersRepository.checkSubscription(iid, uid, getCurrentUserID(), error, subscribeAction, unsubscribeAction)
    }
}