package com.example.amadzon2

data class InterestedUser (var id:String, var nickname:String, val email:String, val rating:Float, val nratings:Int){
    var localImagePath = ""

    constructor() :this("","", "", 0.0f, 0){
    }
}
