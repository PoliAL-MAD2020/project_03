package com.example.amadzon2


import android.util.Log
import androidx.core.net.toUri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import java.io.File


object UserRepository {

    var user: User = User("", "", "", "", "", 0, 0.0f, 0)
    private val idProduct = MutableLiveData<Int>()
    private val db = FirebaseFirestore.getInstance()
    private val mStorageRef: StorageReference = FirebaseStorage.getInstance().reference


    fun getIdProduct(): LiveData<Int>{
        return idProduct
    }

    fun triggerId(uid : String){
        db.collection("user").document(uid).get().addOnSuccessListener { document ->
            if (document!!.exists()) {
                idProduct.value = document.data?.get("lastIdProd").toString().toInt()
            }
        }
    }

    fun getLocation(usr : String, ok :(s :String) ->Unit, error :() -> Unit){
        db.collection("user").whereEqualTo("id", usr).get()
            .addOnSuccessListener {
                for (doc in it.documents) {
                    doc?.data?.get("location")?.toString()?.let { it1 -> ok(it1) }
                }
            }
            .addOnFailureListener { error() }
    }

    fun getUserData(): MutableLiveData<User> {
        val data: MutableLiveData<User> = MutableLiveData()
        data.value = user
        return data
    }

    fun updateLastProduct(id: Int, user: String){
        db.collection("user").document(user).update("lastIdProd", id).addOnFailureListener {
            Log.w("aMADzon", "Last product of the user was not recorded")
        }.addOnSuccessListener {
            Log.w("aMADzon", "Last product of the user is recorded")
        }
    }

    fun checkUser(uid: String, login: ()-> Unit, register : () -> Unit, error: () -> Unit,
                  initDB :(uid :String, ok :() -> Unit, error :() -> Unit) -> Unit) {
        db.collection("user").document(uid).get()
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val document = it.result
                    if (document!!.exists()) {
                        idProduct.value = document.data?.get("lastIdProd").toString().toInt()
                        initDB(uid, login, error)
                    }
                    else register()
                }
                else error()
            }
            .addOnFailureListener {
                error()
            }
    }

    fun getUser(uid: String, init :() -> Unit, ok :() ->Unit, errorDB :() -> Unit) {
        db.collection("user").document(uid).get()
            .addOnSuccessListener { res ->
                res.toObject(User::class.java)?.let {
                    user.full_name = it.full_name
                    user.email = it.email
                    user.nickname = it.nickname
                    user.location = it.location
                    user.id = it.id
                    user.lastIdProd = it.lastIdProd
                    user.nratings = it.nratings
                    user.rating = it.rating
                    user.localImagePath = ""
                    user.newImagePath = ""
                }

                val imageRef = mStorageRef.child("user/${user.id}.jpg")
                val localFile: File = File.createTempFile(user.id, ".jpg")
                imageRef.getFile(localFile)
                    .addOnSuccessListener {
                        user.localImagePath = localFile.absolutePath
                        user.newImagePath = localFile.absolutePath
                        init()
                        ok()
                    }
                    .addOnFailureListener {
                        init()
                        localFile.delete()
                        ok()
                    }
            }
            .addOnFailureListener {
                errorDB()
            }
    }

    fun setUser(uid: String, u: User, ok: ()-> Unit, error: () -> Unit, init :() -> Unit) {
        db.collection("user").document(uid).set(u)
            .addOnSuccessListener {
                user.full_name = u.full_name
                user.email = u.email
                user.nickname = u.nickname
                user.location = u.location
                init()
                ok()
            }
            .addOnFailureListener {
                error()
            }
    }

    fun updateUser(uid: String, u: User, ok: ()-> Unit, error: () -> Unit, init :() -> Unit) {
        db.collection("user").document(uid).update(
            "full_name", u.full_name,
            "nickname", u.nickname,
            "location", u.location,
            "email", u.email
        ).addOnSuccessListener {
            user.full_name = u.full_name
            user.email = u.email
            user.nickname = u.nickname
            user.location = u.location
            init()
            ok()
        }
            .addOnFailureListener {
                error()
            }
    }

    fun updateImage(id: String, path: String, ok: () -> Unit, error: () -> Unit) {
        val file = File(path).toUri()
        val imageRef: StorageReference = mStorageRef.child("user/$id.jpg")
        imageRef.putFile(file)
            .addOnSuccessListener {
                ok()
            }
            .addOnFailureListener {
                error()
            }
    }

    fun getImage (id :String, data: () -> Unit, ok: () -> Unit, error: () -> Unit) {
        val imageRef = mStorageRef.child("user/${id}.jpg")
        val localFile: File = File.createTempFile(id, ".jpg")
        imageRef.getFile(localFile)
            .addOnSuccessListener {
                user.localImagePath = localFile.absolutePath
                user.newImagePath = localFile.absolutePath
                data()
                ok()
            }
            .addOnFailureListener {
                localFile.delete()
                error()
            }
    }

    fun getPairList(uid: String, ok: (id :String, go_on:() -> Unit, error :() ->Unit) -> Unit,
                  error: () -> Unit, go_on :() -> Unit) {
        var docId = ""
        db.collection("user").whereEqualTo("id", uid).get()
            .addOnSuccessListener { it ->
                for (document in it.documents) { // used only once actually
                    docId = document.id
                }
                ok(docId, go_on, error)
            }
            .addOnFailureListener {
                error()
            }
    }

    fun insertInterestedItem(iid: Int, owner: String, uid: String, ok: () -> Unit, error: () -> Unit){
        db.collection("items").whereEqualTo("user", owner).whereEqualTo("id", iid).whereEqualTo("state", VALID).get()
            .addOnSuccessListener {
                for(doc in it.documents ){ //actually done only once
                    val m = hashMapOf("id" to iid, "owner" to owner )
                    db.collection("user").document(uid).collection("interestedItems").document().set(m)
                        .addOnSuccessListener { ok() }
                        .addOnFailureListener { error() }
                }
            }
            .addOnFailureListener { error() }
    }

    fun deleteInterestedItem(iid: Int, owner: String, uid: String, ok: () -> Unit, error: () -> Unit){
        db.collection("user").whereEqualTo("id", uid).get()
            .addOnSuccessListener {
                for(doc in it.documents ){ //actually done only once
                    db.collection("user").document(doc.id).collection("interestedItems")
                        .whereEqualTo("id" , iid)
                        .whereEqualTo("owner", owner).get()
                        .addOnSuccessListener {
                            for(d in it.documents){
                                deleteItemFromUser(doc.id, d.id, ok, error)
                            }
                        }
                        .addOnFailureListener { error() }
                }
            }
            .addOnFailureListener { error() }
    }

    private fun deleteItemFromUser(docUId : String, docIId: String, ok: () -> Unit, error: () -> Unit){
        db.collection("user").document(docUId).collection("interestedItems").document(docIId).delete()
            .addOnSuccessListener { ok() }
            .addOnFailureListener { error() }

    }

    fun getInterestedList(id : String) : CollectionReference {
        return db.collection("user").document(id).collection("interestedItems")
    }

    fun addRating(sID :String, bID :String, evaluation :Float, comment :String, ok: () -> Unit, error: () -> Unit) { //TODO
        db.collection("user").document(bID).get()
            .addOnSuccessListener { ds ->
                ds.toObject(User::class.java)?.let {user ->
                    val nickname = user.nickname
                    val eval = Evaluation(sID, bID, nickname, evaluation, comment)
                    db.collection("user").document(sID).collection("evaluations").add(eval)

                    db.collection("user").document(sID).get()
                        .addOnSuccessListener { ds2 ->
                            ds2.toObject(User::class.java)?.let {
                                var rating = it.rating
                                var nratings = it.nratings
                                rating += evaluation
                                nratings += 1
                                db.collection("user").document(sID).update(
                                    "rating", rating,
                                    "nratings", nratings
                                ).addOnSuccessListener {
                                    ok()
                                }
                                .addOnFailureListener {
                                    error()
                                }
                            }
                        }
                        .addOnFailureListener {
                            error()
                        }
                }
            }
            .addOnFailureListener {
                error()
            }
    }

    fun getRatings(uid :String): CollectionReference {
        return db.collection("user").document(uid).collection("evaluations")
    }

    fun reset() {
        user =  User("", "", "", "", "", 0, 0.0f, 0)
    }

}