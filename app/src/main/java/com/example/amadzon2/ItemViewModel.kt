package com.example.amadzon2

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.Timestamp
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import java.io.File

class ItemViewModel : ViewModel() {

    private val TAG = "aMADzon"
    private var itemListOfOthers = MutableLiveData<ArrayList<Item>>()
    private var itemListMine = MutableLiveData<ArrayList<Item>>()
    private var thisItem = MutableLiveData<Item> ()
    private val mStorageRef: StorageReference = FirebaseStorage.getInstance().reference
    var newImagePath = ""

    init {
        /*listenToMyItems()
        listenToOtherItems()*/
    }

    fun getUser() : String{
        return ItemRepository.getUser()
    }

    fun initItemLD() {
        val item = Item()
        thisItem.value = item
        newImagePath = ""
    }

    fun initItemLD(item:Item){
        thisItem.value = item
        newImagePath = item.localImagePath
    }

    fun initItemLD(iid :Int, uid :String) {
        ItemRepository.getItemNV(uid, iid).get().addOnSuccessListener {qs ->
            var item: Item?
            for(document in qs.documents) { //should be executed only once
                val x =document.data!!
                val category = x["category"].toString()
                val category_index =  x["category_index"].toString().toInt()
                val date = x["date"] as Timestamp
                val description = x["description"].toString()
                val location = x["location"].toString()
                var image = x["localImagePath"].toString()
                val name = x["name"].toString()
                val state = x["state"].toString().toInt()
                val price = x["price"].toString().toDouble()
                val subCategoryIndex = x["sub_category_index"].toString().toInt()
                val buyer = x["buyerID"].toString()
                val rated = x["rated"].toString().toBoolean()
                item = Item(iid, name, price, description, category, uid, category_index, subCategoryIndex, location, date, state, image, buyer, rated)
                thisItem.value = item
                if (image != "") {
                    val imageRef = mStorageRef.child("item/${uid}/${iid}.jpg")
                    val localFile = File.createTempFile("${uid}::${iid}", ".jpg")
                    imageRef.getFile(localFile)
                        .addOnSuccessListener {
                            image = localFile.absolutePath
                            item = Item(iid, name, price, description, category, uid, category_index, subCategoryIndex, location, date, state, image, buyer, rated)
                            thisItem.value = item
                        }
                        .addOnFailureListener {
                            localFile.delete()
                            item = Item(iid, name, price, description, category, uid, category_index, subCategoryIndex, location, date, state, image, buyer, rated)
                            thisItem.value = item
                        }
                }
            }
        }
    }

    fun getItemLD (): MutableLiveData<Item> {
        return thisItem
    }

    fun writeItemToDatabase(item : Item, ok: () -> Unit, error: () -> Unit){
        ItemRepository.writeItem(item).addOnSuccessListener {
            UserRepository.updateLastProduct(item.id, item.user)
            thisItem.value = item
            ok()
        }.addOnFailureListener{
            error()
        }
    }

    fun updateItemToDatabase(item: Item, ok: () -> Unit, error: () -> Unit) {
        ItemRepository.updateItem(item, ok, error)
        thisItem.value = item
    }

    fun getItemsOfTheUser(): LiveData<ArrayList<Item>> {
        Log.w(TAG, "Il mio utente è ${getUser()}")
        return itemListMine
    }

    fun getItemsOfOtherUsers(): LiveData<ArrayList<Item>> {
        return itemListOfOthers
    }

    fun sellItem(user : String, productId : Int, buyer :String, success: () -> Unit){
        ItemRepository.sellItem(user, productId, buyer, success,
            {Log.w("aMADzon", "item was sold")} )
    }

    fun invalidateItem(user: String, id:Int, success: () -> Unit){
        ItemRepository.changeStateAds(user, id, LOCKED,
            {success()},
            {Log.w("aMADzon", "item was not removed")} )
    }


    fun listenToItem(user: String, id:Int) : LiveData<Item>{
        val myItem = MutableLiveData<Item>()
        ItemRepository.getItem(user, id).addSnapshotListener { snapshot, e ->
            if(e != null){
                Log.w(TAG, "exception occurred in the snapshot listener ${e.code}")
                return@addSnapshotListener
            }
            if(snapshot != null){
                snapshot.documents.forEach{
                    val i = it.toObject(Item::class.java)
                    if(i != null){
                        myItem.value = i
                    }
                }
            }
        }
        return myItem
    }

    private fun listenToMyItems() {
        ItemRepository.getItems().addSnapshotListener{
            snapshot, e ->
            if(e != null){
                Log.w(TAG, "exception occurred in the snapshot listener ${e.code}")
                return@addSnapshotListener
            }
            if(snapshot != null){
                val itemFromMe = ArrayList<Item>()
                val doc = snapshot.documents
                Log.w(TAG, "Snapshot is ok")
                doc.forEach {
                    Log.w(TAG, "i'm inside document ${it.id}")
                    val i = it.toObject(Item::class.java)
                    if(i != null){
                        if(i.date > Timestamp.now()) {
                            if (i.user == getUser()) {
                                if (i.localImagePath == "") {
                                    Log.w(TAG, "document is mine: ${getUser()}")
                                    itemFromMe.add(i)
                                    itemListMine.value = itemFromMe
                                }
                                else {
                                    val imageRef = mStorageRef.child("item/${i.user}/${i.id}.jpg")
                                    val localFile = File.createTempFile("${i.user}::${i.id}", ".jpg")
                                    imageRef.getFile(localFile)
                                        .addOnSuccessListener {
                                            i.localImagePath = localFile.absolutePath
                                            itemFromMe.add(i)
                                            itemListMine.value = itemFromMe
                                        }
                                        .addOnFailureListener {
                                            localFile.delete()
                                            itemFromMe.add(i)
                                            itemListMine.value = itemFromMe
                                        }
                                }
                            }
                        }
                    }
                }
                itemListMine.value = itemFromMe
            }
        }
    }

    fun invalidateMyItems(){
        listenToMyItems()
    }

    fun invalidateOtherItems() {
        listenToOtherItems()
    }

    private fun listenToOtherItems() {
        ItemRepository.getItems().addSnapshotListener{
                snapshot, e ->
            if(e != null){
                Log.w(TAG, "exception occurred in the snapshot listener ${e.code}")
                return@addSnapshotListener
            }
            if(snapshot != null){
                val itemFromOthers = ArrayList<Item>()
                val doc = snapshot.documents
                Log.w(TAG, "Snapshot is ok")
                doc.forEach {
                    Log.w(TAG, "i'm inside document ${it.id}")
                    val i = it.toObject(Item::class.java)
                    if(i != null){
                        if(i.state == VALID && i.date > Timestamp.now()) {
                            if (i.user != getUser()) {
                                if( i.localImagePath == "") {
                                    Log.w(TAG, "document is mine")
                                    itemFromOthers.add(i)
                                    itemListOfOthers.value = itemFromOthers
                                }
                                else {
                                    val imageRef = mStorageRef.child("item/${i.user}/${i.id}.jpg")
                                    val localFile = File.createTempFile("${i.user}::${i.id}", ".jpg")
                                    imageRef.getFile(localFile)
                                        .addOnSuccessListener {
                                            i.localImagePath = localFile.absolutePath
                                            itemFromOthers.add(i)
                                            itemListOfOthers.value = itemFromOthers
                                        }
                                        .addOnFailureListener {
                                            localFile.delete()
                                            itemFromOthers.add(i)
                                            itemListOfOthers.value = itemFromOthers
                                        }
                                }
                            }
                        }
                    }
                }

                itemListOfOthers.value = itemFromOthers
            }
        }
    }

    fun updateImage(path: String, id: Int, user: String, item: Item, ok :(item :Item) -> Unit, error :() -> Unit) {
        ItemRepository.setImage(path, id, user, item, ok, error)
    }

    fun evaluateSeller(evaluation: Float, comment :String, ok: () -> Unit, error: () -> Unit){
        UserRepository.addRating(thisItem.value!!.user, getUser(), evaluation, comment, ok, error)
        ItemRepository.setEvaluation(thisItem.value!!.user, thisItem.value!!.id)
    }
}