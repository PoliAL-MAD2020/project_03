package com.example.amadzon2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_evaluation_list.*

class EvaluationsListFragment : Fragment(){
    private var navController: NavController? = null
    private var adapter: EvaluationAdapter? = null
    private var notification : Boolean = false
    private lateinit var evaluationList : ArrayList<Evaluation>
    private lateinit var vm : UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)
        notification = requireArguments().getBoolean("group06.lab4.NOTIFICATION")
        if(notification)
            vm.initEvalList(requireArguments().getString("group06.lab4.USERID")!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_evaluation_list, container, false)
        evaluationList = ArrayList()

        (rootView.findViewById(R.id.evalList) as RecyclerView).also {
            adapter = setupAdapter(it, evaluationList)
        }
        return rootView
    }

    private fun setupAdapter(v: RecyclerView, a: ArrayList<Evaluation>) : EvaluationAdapter {
        v.layoutManager = LinearLayoutManager(activity)
        val ad = EvaluationAdapter(a)
        v.adapter = ad
        return ad
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        vm.getEvalList().observe(viewLifecycleOwner, Observer{ evaluations ->
            progressbar_el.hide()
            evaluationList.removeAll(evaluationList)
            evaluationList.addAll(evaluations)
            adapter!!.evalFullList.clear()
            adapter!!.evalFullList.addAll(evaluations)
            adapter!!.notifyDataSetChanged()
        })
    }

}