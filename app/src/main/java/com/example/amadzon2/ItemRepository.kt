package com.example.amadzon2

import androidx.core.net.toUri
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import java.io.File

object ItemRepository {

    private var db = FirebaseFirestore.getInstance()
    private val mStorageRef: StorageReference = FirebaseStorage.getInstance().reference
    private val interestedItem =  ArrayList<Item>()

    fun getUser() : String {
        return FirebaseAuth.getInstance().currentUser?.uid.toString()
    }

    fun writeItem(i: Item) : Task<Void> {
        val coll = db.collection("items")
        return coll.document().set(i)
    }

    fun getItems() :CollectionReference{
        return db.collection("items")
    }

    fun getItem(user: String, id:Int) : Query{
        return db.collection("items").whereEqualTo("id", id)
            .whereEqualTo("user",user).whereEqualTo("state", VALID)
    }

    fun getItemNV(user: String, id:Int) : Query{
        return db.collection("items").whereEqualTo("id", id)
            .whereEqualTo("user",user)
    }

    fun getAnItem(user: String, id:Int, success: () -> Unit, error: () -> Unit){
        db.collection("items").whereEqualTo("id", id)
            .whereEqualTo("user",user).get()
            .addOnSuccessListener {
                for(d in it.documents){//actually called only once
                    val i = d.toObject(Item::class.java)
                    if(i != null){
                        if(i.state == VALID) {
                            if (i.localImagePath == "") {
                                interestedItem.add(i)
                                success()
                            } else {
                                val imageRef = mStorageRef.child("item/${i.user}/${i.id}.jpg")
                                val localFile = File.createTempFile("${i.user}::${i.id}", ".jpg")
                                imageRef.getFile(localFile)
                                    .addOnSuccessListener {
                                        i.localImagePath = localFile.absolutePath
                                        interestedItem.add(i)
                                        success()
                                    }
                                    .addOnFailureListener {
                                        localFile.delete()
                                        interestedItem.add(i)
                                        success()
                                    }
                            }
                        } else {
                            success()
                        }
                    }
                    success()
                }
            }
            .addOnFailureListener {
                error()
            }
    }

    fun getIIList() : ArrayList<Item>{
        return interestedItem
    }

    fun clearInterestedItem() {
        interestedItem.clear()
    }

    fun changeStateAds(user: String, id:Int, newState:Int, removed:() -> Unit, error: () -> Unit){
        db.collection("items").whereEqualTo("user", user).whereEqualTo("id", id).whereEqualTo("state", VALID).get()
            .addOnSuccessListener {
                for(doc in it.documents){
                    if(doc.data != null) {
                        db.collection("items").document(doc.id).update("state", newState)
                            .addOnSuccessListener { removed() }
                            .addOnFailureListener { error() }
                    }
                }
            }
            .addOnFailureListener { error() }
    }

    fun updateItem(i: Item, ok: () -> Unit, error: () -> Unit){
        val q = db.collection("items").whereEqualTo("user", getUser())
            .whereEqualTo("id", i.id).whereEqualTo("state", VALID)
        q.get().addOnSuccessListener { documents ->
            for (document in documents) {
                //actually is done only once
                val doc = db.collection("items").document(document.id)
                doc.update(
                    "category", i.category,
                    "category_index", i.category_index,
                    "date", i.date,
                    "description", i.description,
                    "location", i.location,
                    "name", i.name,
                    "price", i.price,
                    "sub_category_index", i.sub_category_index,
                    "localImagePath", i.localImagePath
                ).addOnSuccessListener {
                    ok()
                }.addOnFailureListener{
                    error()
                }
            }
        }.addOnFailureListener{
            error()
        }


    }

    fun setImage(path: String, id: Int, user: String, item :Item, ok :(item :Item) ->Unit, error :() ->Unit) {
        val file = File(path).toUri()
        val imageRef: StorageReference = mStorageRef.child("item/$user/$id.jpg")
        imageRef.putFile(file)
            .addOnSuccessListener {
                val localFile = File.createTempFile("$user::$id", ".jpg")
                imageRef.getFile(localFile)
                    .addOnSuccessListener {
                        item.localImagePath = localFile.absolutePath
                        ok(item)
                    }
                    .addOnFailureListener {
                        error()
                    }
            }
            .addOnFailureListener {
                error()
            }
    }

    fun sellItem(userId: String, productId: Int, buyer: String, ok: () -> Unit, error: () -> Unit) {
        db.collection("items").whereEqualTo("user", userId).whereEqualTo("id", productId).whereEqualTo("state", VALID).get()
            .addOnSuccessListener {
                for(doc in it.documents){
                    if(doc.data != null) {
                        db.collection("items").document(doc.id).update(
                            "state", SOLD,
                            "buyerID", buyer
                        )
                            .addOnSuccessListener { ok() }
                            .addOnFailureListener { error() }
                    }
                }
            }
            .addOnFailureListener { error() }
    }

    fun setEvaluation(uId :String, pId :Int){
        db.collection("items").whereEqualTo("user", uId).whereEqualTo("id", pId).whereEqualTo("state", SOLD).get()
            .addOnSuccessListener {
                for(doc in it.documents){
                    if(doc.data != null) {
                        db.collection("items").document(doc.id).update("rated", true)
                    }
                }
            }
    }

}