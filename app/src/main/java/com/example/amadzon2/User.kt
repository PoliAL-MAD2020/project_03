package com.example.amadzon2

data class User (var full_name:String, var email:String, var nickname:String, var location:String,
                 var id:String, var lastIdProd:Int, var rating :Float, var nratings :Int) {
    var localImagePath = ""
    var newImagePath = ""

    constructor() :this("","","", "", "", 0, 0.0f, 0){
    }

}