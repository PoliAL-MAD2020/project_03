package com.example.amadzon2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.util.*
import kotlin.collections.ArrayList

class EvaluationAdapter (var evaluations :ArrayList<Evaluation>): RecyclerView.Adapter<EvaluationAdapter.EvaluationViewHolder>(), Filterable {

    var evalFullList = ArrayList(evaluations)

    override fun getItemCount(): Int = evaluations.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EvaluationViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.evaluation, parent, false)
        return EvaluationViewHolder(v)
    }

    override fun onBindViewHolder(holder: EvaluationViewHolder, position: Int) {
        holder.bind(evaluations[position])
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(charSequence: CharSequence?, filterResults: FilterResults) {
                @Suppress("UNCHECKED_CAST")
                evaluations = filterResults.values as ArrayList<Evaluation>
                notifyDataSetChanged()
            }

            override fun performFiltering(charSequence: CharSequence?): FilterResults {
                val queryString = charSequence?.toString()?.toLowerCase(Locale.ROOT)

                val filterList = ArrayList<Evaluation>()
                val filterResults = FilterResults()

                if (queryString==null || queryString.isEmpty() || queryString == "")
                    filterList.addAll(evalFullList)
                else {
                    filterList.addAll(evalFullList.filter {
                        it.buyerNickname.toLowerCase(Locale.ROOT).contains(queryString) ||
                        it.comment.toLowerCase(Locale.ROOT).contains(queryString)

                    })
                }
                filterResults.values = filterList
                filterResults.count = filterList.count()
                return filterResults
            }
        }
    }


    inner class EvaluationViewHolder(v: View): RecyclerView.ViewHolder(v) {
        private val name :TextView = v.findViewById(R.id.eval_nickname)
        private val rating :RatingBar = v.findViewById(R.id.eval_rating)
        private val comment :TextView = v.findViewById(R.id.eval_comment)
        private val number :TextView = v.findViewById(R.id.eval_ratenum)

        fun bind(i:Evaluation) {
            name.text = i.buyerNickname
            rating.rating = i.rating
            number.text = rating.rating.toString()
            if (i.comment != "")
                comment.text = i.comment
            else comment.visibility = View.GONE
        }
    }
}