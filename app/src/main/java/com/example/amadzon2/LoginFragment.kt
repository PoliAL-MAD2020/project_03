package com.example.amadzon2

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : Fragment() {
    lateinit var navController : NavController

    lateinit var vm :UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            requireActivity().finish()
        }
        vm = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).hideDrawer()

        navController = Navigation.findNavController(view)
        signin_button.setOnClickListener { signIn() }
    }

    override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseAuth.getInstance().let {
            vm.initUser(vm.getCurrentUserID(),
                it.currentUser?.displayName.toString(),
                it.currentUser?.email.toString(),
                ::navigateLogin, ::navigateRegister, ::errorMessageDB)
            progress_circular.visibility = View.VISIBLE
        }
    }

    private fun signIn() {
        progress_circular.show()
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.web_client_id))
            .requestEmail()
            .build()
        val signInIntent = GoogleSignIn.getClient(requireActivity(), gso).signInIntent
        startActivityForResult(signInIntent, 9001)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == 9001) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)!!
                firebaseAuthWithGoogle(account.idToken!!, account.displayName.toString(), account.email.toString())
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.w("aMADzon", "Google sign in failed", e)
                progress_circular.hide()
                Snackbar.make(requireView(), "Google Authentication Failed", Snackbar.LENGTH_SHORT).show()
            }
        }
    }

    private fun firebaseAuthWithGoogle(idToken: String, name:String, email:String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        FirebaseAuth.getInstance().signInWithCredential(credential)
            .addOnCompleteListener(requireActivity()) { task ->
                if (task.isSuccessful) {
                    vm.initUser(vm.getCurrentUserID(), name, email,
                        ::navigateLogin, ::navigateRegister, ::errorMessageDB)
                } else {
                    // If sign in fails, display a message to the user.
                    Snackbar.make(requireView(), "Authentication Failed", Snackbar.LENGTH_SHORT).show()
                    progress_circular?.hide()
                }
            }
    }

    private fun navigateLogin() {
        navController.navigate(R.id.action_loginFragment_to_onSaleListFragment)
        progress_circular?.hide()
        Snackbar.make(requireView(), "Welcome back ${FirebaseAuth.getInstance().currentUser?.displayName}",
            Snackbar.LENGTH_LONG).show()
        (activity as MainActivity).apply { showDrawer(); updateDrawer() }
    }

    private fun navigateRegister() {
        navController.navigate(R.id.action_loginFragment_to_userInfoFragment)
        progress_circular?.hide()
    }

    private fun errorMessageDB() {
        progress_circular?.hide()
        Log.i("aMADzon", "DB error")
    }
}
