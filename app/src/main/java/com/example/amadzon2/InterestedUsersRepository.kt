package com.example.amadzon2

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import java.io.File

object InterestedUsersRepository {

    private val db = FirebaseFirestore.getInstance()
    var map = HashMap<String, InterestedUser>()
    private val mStorageRef: StorageReference = FirebaseStorage.getInstance().reference

    fun getIdList(iid:Int, uid: String, ok: (id :String, go_on:() -> Unit, error :() ->Unit) -> Unit,
                  error: () -> Unit, go_on :() -> Unit) {
        var docId = ""
        db.collection("interestedUsers").whereEqualTo("id", iid).whereEqualTo("user", uid).get()
            .addOnSuccessListener { it ->
                for (document in it.documents) {
                    docId = document.id
                }
                ok(docId, go_on, error)
            }
            .addOnFailureListener {
                error()
            }
    }

    fun getUserList(id :String) :CollectionReference {
        return db.collection("interestedUsers").document(id).collection("users")
    }

    fun clearInterestedCollection(){
        map.clear()
    }

    fun getUser(id: String, success: () -> Unit, error: () -> Unit) {
        db.collection("user").document(id).get()
            .addOnSuccessListener { res ->
                val u = res.toObject(User::class.java)
                if(u != null) {
                    val int_u = InterestedUser(id, u.nickname, u.email, u.rating, u.nratings)
                    //list.add(int_u)
                    map[int_u.id] = int_u

                    val imageRef = mStorageRef.child("user/${int_u.id}.jpg")
                    val localFile: File = File.createTempFile(int_u.id, ".jpg")
                    imageRef.getFile(localFile)
                        .addOnSuccessListener {
                            int_u.localImagePath = localFile.absolutePath
                            success()
                        }
                        .addOnFailureListener {
                            localFile.delete()
                            success()
                        }

                } else {
                    Log.w("aMADzon", "id is $id")
                }
            }
            .addOnFailureListener {
                error()
            }
    }

    fun getList(): MutableLiveData<ArrayList<InterestedUser>> {
        val data: MutableLiveData<ArrayList<InterestedUser>> = MutableLiveData()
        data.value = ArrayList(map.values)
        return data
    }

    fun checkSubscription(iid:Int, uid:String, sub_uid: String, error: () -> Unit,
                         setFabSub :() -> Unit, setFabUnsub :() -> Unit) {
        val doc = db.collection("interestedUsers").whereEqualTo("id", iid).whereEqualTo("user", uid)
        doc.get().addOnSuccessListener { it ->
            for (document in it.documents) {
                db.collection("interestedUsers").document(document.id).collection("users").document(sub_uid).get().addOnSuccessListener {
                   if (it!!.exists()) {
                       setFabSub()
                   }
                   else {
                       setFabUnsub()
                   }
                }.addOnFailureListener {
                    error()
                }
            }
        }
            .addOnFailureListener {
                error()
            }
    }

    fun addIntUser(iid:Int, uid:String, sub_uid: String, ok: () -> Unit, error: () -> Unit) {
        val doc = db.collection("interestedUsers").whereEqualTo("id", iid).whereEqualTo("user", uid)
        doc.get().addOnSuccessListener { it ->
            if(it.isEmpty) {
                val d = db.collection("interestedUsers").document()
                d.set(hashMapOf("id" to iid, "user" to uid))
                d.collection("users").document(sub_uid).set(hashMapOf("sub_id" to sub_uid))
                    .addOnSuccessListener {
                        ok()
                    }
                    .addOnFailureListener {
                        error()
                    }
            } else {
                for (document in it.documents) {         //actually is done only once
                    val x = db.collection("interestedUsers").document(document.id).collection("users").document(sub_uid)
                        x.set(hashMapOf("sub_id" to sub_uid))
                        .addOnSuccessListener {
                            ok()
                        }
                        .addOnFailureListener {
                            error()
                        }
                }
            }
        }.addOnFailureListener {//if the document doesn't yet exists
            error()
        }
    }

    fun removeIntUser(iid: Int, uid: String, sub_uid: String, ok: () -> Unit, error: () -> Unit) {
        val doc = db.collection("interestedUsers").whereEqualTo("id", iid).whereEqualTo("user", uid)
        doc.get().addOnSuccessListener { it ->
            for (document in it.documents) {         //actually is done only once
                db.collection("interestedUsers").document(document.id).collection("users")
                    .document(sub_uid).delete()
                    .addOnSuccessListener {
                        ok()
                    }
                    .addOnFailureListener {
                        error()
                    }
            }
        }
            .addOnFailureListener {
                error()
            }
    }
}