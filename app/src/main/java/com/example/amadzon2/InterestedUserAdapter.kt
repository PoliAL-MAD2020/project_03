package com.example.amadzon2

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.io.File
import java.util.*

class InterestedUserAdapter (val users: ArrayList<InterestedUser>): RecyclerView.Adapter<InterestedUserAdapter.InterestedUserViewHolder>() {

    var onItemClick: ((InterestedUser) -> Unit)? = null
    var onButtonClick: ((InterestedUser) -> Unit)? = null

    override fun getItemCount(): Int = users.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InterestedUserViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.interested_user, parent, false)
        return InterestedUserViewHolder(v)
    }

    override fun onBindViewHolder(holder: InterestedUserViewHolder, position: Int) {
        holder.bind(users[position])
    }

    inner class InterestedUserViewHolder(v: View): RecyclerView.ViewHolder(v) {
        private val name: TextView = v.findViewById(R.id.user_name)
        private val img: ImageView = v.findViewById(R.id.user_image)
        private var sellButton: ImageView = v.findViewById(R.id.sell_button)

        init {
            v.setOnClickListener {
                onItemClick?.invoke(users[adapterPosition])
            }
            sellButton.setOnClickListener {
                onButtonClick?.invoke(users[adapterPosition])
            }
        }

        fun bind(i:InterestedUser) {
            name.text = i.nickname
            if (i.localImagePath == "")
                img.setImageResource(R.drawable.user)
            else img.setImageURI(Uri.fromFile(File(i.localImagePath)))
        }
    }
}