package com.example.amadzon2

import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.activity.addCallback
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.fragment_item_list.*
import java.io.FileNotFoundException

class OnSaleListFragment : Fragment () {

    private var navController: NavController? = null
    private var adapter: ItemAdapter? = null
    private lateinit var itemList : ArrayList<Item>
    private lateinit var vm : ItemViewModel
    private val myItems : Boolean  = false
    private var queryString = ""
    private lateinit var uvm : UserViewModel
    private lateinit var id : String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            requireActivity().finish()
        }
        uvm = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)
        id = uvm.getCurrentUserID()
        FirebaseMessaging.getInstance().subscribeToTopic("/topics/topic_$id")
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)
        val item = menu.findItem(R.id.search_menu)
        val search = item.actionView as SearchView
        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                queryString = query.toString()
                adapter!!.filter.filter(queryString)
                return true
            }
            override fun onQueryTextChange(newText: String): Boolean {
                queryString = newText
                adapter!!.filter.filter(queryString)
                return true
            }
        })
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_item_list, container, false)

        itemList = ArrayList()
        vm = ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)
        vm.invalidateOtherItems()

        (rootView.findViewById(R.id.itemList) as RecyclerView).also {
            adapter = setupAdapter(it, itemList)
        }

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        setHasOptionsMenu(true)

        val str = "No item on sale yet"
        emptyListMessage.text = str

        vm.getItemsOfOtherUsers().observe(viewLifecycleOwner, Observer{
                items ->
            progressbar_il.hide()
            itemList.removeAll(itemList)
            itemList.addAll(items)
            adapter!!.itemListFull.clear()
            adapter!!.itemListFull.addAll(items)
            adapter!!.notifyDataSetChanged()
            if(items.size != 0)
                hideMessage()
            else
                displayMessage()
        })

        adapter!!.onItemClick = { item ->
            val bundle = bundleOf(
                "group06.lab3.PRODUCT_ID" to item.id.toString(),
                "group06.lab3.MYITEMS" to myItems,
                "group06.lab3.OWNER" to item.user,
                "group06.lab3.MODIFIED" to false,
                "group06.lab3.STATE" to item.state
            )
            vm.initItemLD(item)
            navController!!.navigate(R.id.action_onSaleListFragment_to_itemDetailsFragment, bundle)
        }

        /*if(adapter!!.itemCount != 0) {
            hideMessage(view)
        } else {
            displayMessage(view)
        }*/

        fab.visibility = View.GONE
    }

    private fun setupAdapter(v: RecyclerView, a: ArrayList<Item>) : ItemAdapter {
        v.layoutManager = LinearLayoutManager(activity)
        val ad = ItemAdapter(a, false)
        v.adapter = ad
        return ad
    }

    private fun isValidUri(uri: String): Boolean {
        return try {
            requireActivity().contentResolver.openInputStream(Uri.parse(uri))
            true
        } catch (e: FileNotFoundException) {
            false
        }
    }

    private fun hideMessage() {
        emptyList.visibility = View.GONE
    }

    private fun displayMessage() {
        emptyList.visibility = View.VISIBLE
    }
}