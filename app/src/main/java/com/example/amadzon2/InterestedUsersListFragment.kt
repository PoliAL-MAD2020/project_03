package com.example.amadzon2

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.interested_users_list.*
import org.json.JSONException
import org.json.JSONObject

class InterestedUsersListFragment : Fragment() {

    private lateinit var navController: NavController
    private var adapter: InterestedUserAdapter? = null
    private var productId = -1
    private var owner = ""
    private lateinit var userList : ArrayList<InterestedUser>
    private lateinit var vm : UserViewModel
    private lateinit var vmi : ItemViewModel
    private val FCM_API = "https://fcm.googleapis.com/fcm/send"
    private val serverkey =
        "key=" + "AAAAcaJU9K8:APA91bGR_1DhWaQzLX75hUpdpjlqORLDk4mEeM7DviW9ZjOxetMv0brePRKFjOXpFUjjrzdyPKfKpBTfXAMhDRPanekQUVcrKOhZDb18SjYZMuCqJRa_vZ1iTMhlLjWJRlt7vIELtStB"
    private val contentType = "application/json"
    private val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val x = requireArguments().getString("group06.lab3.PRODUCT_ID")
        productId = x?.toInt()!!
        owner = requireArguments().getString("group06.lab3.OWNER").toString()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootview = inflater.inflate(R.layout.interested_users_list, container, false)

        vm = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)
        vmi = ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)

        userList = ArrayList()

        (rootview.findViewById(R.id.interestedUsersList) as RecyclerView).also {
            adapter = setupAdapter(it, userList)
        }

        return rootview
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        progressbar_iu.show()
        vm.clearInterestedUsers()

        vm.getList().observe(viewLifecycleOwner, Observer{ users ->
            progressbar_iu.hide()
            userList.removeAll(userList)
            userList.addAll(users)
            adapter!!.notifyDataSetChanged()
            if(users.size > 0){
                (view.findViewById(R.id.emptyUserListMessage) as TextView).also {
                    it.visibility = View.GONE
                }
            } else {
                (view.findViewById(R.id.emptyUserListMessage) as TextView).also {
                    it.visibility = View.VISIBLE
                }
            }
        })

        adapter!!.onItemClick = { user ->
            val bundle = bundleOf(  "group06.lab3.OTHER_USER" to true)
            vm.initUser(user.id, ::navigate, bundle, ::error)
        }

        adapter!!.onButtonClick = { user ->
            llProgressBar.visibility = View.VISIBLE
            sellItem(owner, productId, user.id)
            {   llProgressBar.visibility = View.GONE
                navController.navigate(R.id.action_interestedUsersListFragment_to_itemListFragment)
                Snackbar.make(requireView(), "Item correctly sold to ${user.nickname}", Snackbar.LENGTH_SHORT)
                    .setBackgroundTint(Color.parseColor("#4CAF50")).show() }
            sendNotification(user.id)
        }
    }

    private fun sellItem(user : String, productId : Int, buyer :String, success: () -> Unit){
        vmi.sellItem(user, productId, buyer, success)
        for(i in userList){
            vm.unsuscribeToItem(productId, user, i.id, {}, {Log.w("aMADzon", "Error in unsubscribing to an item $productId")})
        }
    }

    private fun setupAdapter(v: RecyclerView, a: ArrayList<InterestedUser>) : InterestedUserAdapter {
        v.layoutManager = LinearLayoutManager(activity)
        val ad = InterestedUserAdapter(a)
        v.adapter = ad
        return ad
    }

    private fun navigate(bundle :Bundle) {
        navController.navigate(R.id.action_interestedUsersListFragment_to_showProfileFragment, bundle)
    }

    private fun error() {
        Snackbar.make(requireView(), "Error", Snackbar.LENGTH_SHORT)
            .setBackgroundTint(Color.parseColor("#C0392B")).show()
    }

    private fun sendNotification(idBuyer: String) {
        val notification = JSONObject()
        val notificationBody = JSONObject()

        for (user in userList) {
            if (user.id != idBuyer) {
                val topic = "/topics/topic_" + user.id
                try {
                    notificationBody.put("type", "S")
                    notificationBody.put("title", "aMADzon")
                    notificationBody.put("message", "An item you showed interest to has been sold!")
                    notificationBody.put("destID", user.id)
                    notification.put("to", topic)
                    notification.put("data", notificationBody)
                } catch (e: JSONException) {
                    Log.e("aMADzon", e.message.toString())
                }

                val jsonObjectRequest = object : JsonObjectRequest(FCM_API, notification,
                    Response.Listener<JSONObject> { response ->
                        Log.i("aMADzon", "onResponse: $response")
                    },
                    Response.ErrorListener {
                        Toast.makeText(context, "Request Error", Toast.LENGTH_LONG).show()
                    }) {
                    override fun getHeaders(): MutableMap<String, String> {
                        val params = HashMap<String, String>()
                        params["Authorization"] = serverkey
                        params["Content-Type"] = contentType
                        return params
                    }
                }
                requestQueue.add(jsonObjectRequest)
            } else {
                val topic = "/topics/topic_" + idBuyer
                try {
                    notificationBody.put("type", "V")
                    notificationBody.put("title", "aMADzon")
                    notificationBody.put("message", "You've succesfully bought an item!")
                    notificationBody.put("destID", idBuyer)
                    notificationBody.put("productID", productId)
                    notificationBody.put("owner", owner)
                    notificationBody.put("state", SOLD)
                    notification.put("to", topic)
                    notification.put("data", notificationBody)
                } catch (e: JSONException) {
                    Log.e("aMADzon", e.message.toString())
                }

                val jsonObjectRequest = object : JsonObjectRequest(FCM_API, notification,
                    Response.Listener<JSONObject> { response ->
                        Log.i("aMADzon", "onResponse: $response")
                    },
                    Response.ErrorListener {
                        Toast.makeText(context, "Request Error", Toast.LENGTH_LONG).show()
                    }) {
                    override fun getHeaders(): MutableMap<String, String> {
                        val params = HashMap<String, String>()
                        params["Authorization"] = serverkey
                        params["Content-Type"] = contentType
                        return params
                    }
                }
                requestQueue.add(jsonObjectRequest)
            }
        }
    }
}