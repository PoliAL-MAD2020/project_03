package com.example.amadzon2

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Matrix
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.InputFilter
import android.text.Spanned
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.activity.addCallback
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.os.bundleOf
import androidx.exifinterface.media.ExifInterface
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.Timestamp
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.fragment_item_edit.*
import java.io.*
import java.text.SimpleDateFormat
import java.util.*

class ItemEditFragment : Fragment() , OnMapReadyCallback{

    private lateinit var navController: NavController
    private var newItem: Boolean = false
    private var productId: Int = -1
    private var owner = ""

    private lateinit var googleMap: GoogleMap
    private var savingTimes = 0
    private lateinit var vm : ItemViewModel
    private lateinit var vmu : UserViewModel
    private lateinit var storage: StorageReference
    private val codeGalleryRequest = 100
    private val codePhotoRequest = 101
    private var actualImageUri = ""
    private var uriString: String = ""
    private var mCurrentPhotoPath: String = ""
    private var backupImgPath: String = ""
    private var backupImgUri: String = ""
    private var isRotating = false
    private var wantToSave = false
    private var fromDetails = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        vm = ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)
        vmu = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)

        newItem = requireArguments().getBoolean("group06.lab3.NEW_PRODUCT_FLAG")
        fromDetails = requireArguments().getBoolean("group06.lab3.FROM_DETAILS")

        if(newItem){
            productId = (activity as MainActivity).getNewItemId()
            owner = vm.getUser()
        } else {
            productId = requireArguments().getInt("group06.lab3.PRODUCT_ID")
            owner = requireArguments().getString("group06.lab3.OWNER").toString()
        }
        storage = FirebaseStorage.getInstance().reference
    }

    private fun getNavOptions(): NavOptions? {
        return NavOptions.Builder()
            .setEnterAnim(R.anim.slide_in_left)
            .setExitAnim(R.anim.slide_out_right)
            .setPopEnterAnim(R.anim.slide_in_right)
            .setPopExitAnim(R.anim.slide_out_left)
            .setPopUpTo(R.id.mobile_navigation, true)
            .build()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_item_edit, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registerForContextMenu(uploadButton)
        uploadButton.setOnClickListener {
            requireActivity().openContextMenu(it)
        }

        category_edit.adapter = setAdapterView(resources.getStringArray(R.array.category_list))

        short_description_edit_txt.setOnFocusChangeListener { _, _ -> short_description_edit.error = null }
        price_edit_txt.setOnFocusChangeListener { _, _ -> price_edit.error = null }
        full_description_edit_txt.setOnFocusChangeListener { _, _ -> full_description_edit.error = null }
        location_edit_txt.setOnFocusChangeListener { _, _ -> location_edit.error = null }

        if(newItem){
            vmu.getUser().observe(requireActivity(), Observer<User> {
                location_edit_txt.text = Editable.Factory.getInstance().newEditable(it.location)
            })
        } else{//updating my screen with value of the item fatched with database
            vm.getItemLD().observe( viewLifecycleOwner, Observer { item ->
                short_description_edit_txt.text = Editable.Factory.getInstance().newEditable(item.name)
                price_edit_txt.text = Editable.Factory.getInstance().newEditable(item.price.toString())
                full_description_edit_txt.text = Editable.Factory.getInstance().newEditable(item.description)
                category_edit.setSelection(item.category_index)
                val list = setSubCategoryList(item.category_index)
                sub_category_edit.adapter = setAdapterView(list)
                sub_category_edit.setSelection(item.sub_category_index)
                location_edit_txt.text = Editable.Factory.getInstance().newEditable(item.location)
                val picker: DatePicker = expiration_date_edit as DatePicker
                val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
                val d = sdf.format(item.date.toDate())
                val date = d.split("/")
                picker.init(date[2].toInt(), date[1].toInt() - 1, date[0].toInt(), null)
            })
        }

        location_edit_txt.setOnEditorActionListener { _, actionId, _ ->
            if(actionId == EditorInfo.IME_ACTION_SEARCH){
                googleMap.clear()
                displayLocation(refactorLocation(location_edit_txt.text.toString()))
                (activity as MainActivity).hideKeyboard()
                true
            } else {
                false
            }
        }

        if (vm.newImagePath != "")
            product_image.setImageURI(Uri.fromFile(File(vm.newImagePath)))
        navController = Navigation.findNavController(view)
        //category_edit.postDelayed( { setSpinner() }, 500) // Alternative solution to prevent unwanted onItemSelected calls from spinner
        category_edit.post { setSpinner() }
        price_edit_txt.filters = Array(1) { setFilter() }
        actualImageUri = uriString
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        val inflater: MenuInflater = requireActivity().menuInflater
        if (navController.currentDestination?.id == R.id.itemEditFragment) {
            inflater.inflate(R.menu.upload_photo_menu, menu)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.save_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(savingTimes >1){
            return false
        }
        return when (item.itemId) {
            R.id.save -> {
                savingTimes++
                saveItem()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        if (requireActivity().packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
            return when (item.itemId) {
                R.id.gallery_menu -> {
                    pickFromGallery()
                    true
                }
                R.id.camera_menu -> {
                    takePhoto()
                    true
                }
                else -> super.onContextItemSelected(item)
            }
        } else {
            // camera not available
            pickFromGallery()
            return true
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt("PRODUCT_ID", productId)
        val date: DatePicker? = requireActivity().findViewById(R.id.expiration_date_edit)
        if (date != null) {
            outState.putInt("picker_day", date.dayOfMonth)
            outState.putInt("picker_month", date.month)
            outState.putInt("picker_year", date.year)
        }
        if (category_edit != null && sub_category_edit != null && category_edit.selectedItemPosition != 0) {
            outState.putInt("category_index", category_edit.selectedItemPosition)
            outState.putInt(("subcategory_index"), sub_category_edit.selectedItemPosition)
        }
        outState.putString("ImageURI", uriString)
        outState.putString("absPath", mCurrentPhotoPath)
        outState.putString("prevPath", backupImgPath)
        outState.putString("prevUri", backupImgUri)
        outState.putString("actualURI", actualImageUri)
        isRotating = true
        super.onSaveInstanceState(outState)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState != null) {
            expiration_date_edit.init(
                savedInstanceState.getInt("picker_year"),
                savedInstanceState.getInt("picker_month"),
                savedInstanceState.getInt("picker_day"),
                null
            )

            category_edit.setSelection(savedInstanceState.getInt("category_index"))
            val list = setSubCategoryList(savedInstanceState.getInt("category_index"))
            sub_category_edit.adapter = setAdapterView(list)
            sub_category_edit.setSelection(savedInstanceState.getInt("subcategory_index"))

            productId = savedInstanceState.getInt("PRODUCT_ID")
            mCurrentPhotoPath = savedInstanceState.getString("absPath").toString()
            backupImgPath = savedInstanceState.getString("prevPath").toString()
            backupImgUri = savedInstanceState.getString("prevUri").toString()
            uriString = savedInstanceState.getString("ImageURI").toString().takeIf { isValidUri(it) } ?: ""
            actualImageUri = savedInstanceState.getString("actualURI").toString()
        }
    }

    private fun isNotEmpty(e: TextInputLayout, message: String): Boolean {
        if (e.editText?.text.toString() == "") {
            e.error = message
            return false
        }
        return true
    }

    private fun saveItem() {
        (activity as MainActivity).hideKeyboard()
        //controllo se ho compilato i campi
        if (!isNotEmpty(short_description_edit, "Enter a title") or
            !isNotEmpty(price_edit, "Enter a price") or
            !isNotEmpty(full_description_edit, "Enter a description") or
            !isNotEmpty(location_edit, "Enter a location")
        ) {
            savingTimes = 0
            return
        }

        if(!displayLocation(refactorLocation(location_edit_txt.text.toString()))) {
            savingTimes = 0
            return
        }

        val tempShortDesc = short_description_edit_txt.text.toString().trim().takeIf { it != "" } ?: ""
        val tempPrice: Double = price_edit_txt.text.toString().trim().takeIf { it != "" }?.toDouble() ?: .0
        val tempFullDesc = full_description_edit_txt.text.toString().trim().takeIf { it != "" } ?: ""
        val tempLocation = location_edit_txt.text.toString().trim().takeIf { it != "" } ?: ""

        if (category_edit.selectedItemPosition == 0) {
            showMessage("Please insert a category", 2)
            savingTimes = 0
            return
        }
        if (sub_category_edit.selectedItem == null || sub_category_edit.selectedItemPosition == 0) {
            showMessage("Please insert a subcategory", 2)
            savingTimes = 0
            return
        }
        val tempCategory = category_edit.selectedItem.toString() + ": " + sub_category_edit.selectedItem.toString()
        val tempCategoryIndex = category_edit.selectedItemPosition
        val tempSubCategoryIndex = sub_category_edit.selectedItemPosition

        val picker: DatePicker = expiration_date_edit as DatePicker
        val tempDay = if (picker.dayOfMonth < 10) "0" + picker.dayOfMonth else picker.dayOfMonth.toString()
        val m = picker.month + 1
        val tempMonth = if (m < 10) "0$m" else m.toString()
        val tempExpDate = "$tempDay/$tempMonth/${picker.year}"

        val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        val strDate: Date = sdf.parse(tempExpDate) ?: Date()
        if (Date().after(strDate)) {
            showMessage("Please insert a valid date", 2)
            savingTimes = 0
            return
        }

        llProgressBar.visibility = View.VISIBLE

        val t = Timestamp(strDate)

        val item = Item(
            productId,
            tempShortDesc,
            tempPrice,
            tempFullDesc,
            tempCategory,
            vm.getUser(),
            tempCategoryIndex,
            tempSubCategoryIndex,
            tempLocation,
            t,
            VALID,
            vm.newImagePath,
            "",
            false
        )
        //now i have the item, i will save it in the database
        if (uriString != actualImageUri)
            vm.updateImage(mCurrentPhotoPath, productId, vm.getUser(), item, ::writeItem, ::errorMessageS)
        else writeItem(item)
    }

    private fun showMessage(s: String, option: Int = 0) {
        val c: String? = when (option) {
            1 -> "#4CAF50" // green
            2 -> "#C0392B" // red
            else -> null
        }
        if (c != null)
            Snackbar.make(requireView(), s, Snackbar.LENGTH_SHORT)
                .setBackgroundTint(Color.parseColor(c)).show()
        else
            Snackbar.make(requireView(), s, Snackbar.LENGTH_SHORT).show()

    }

    private fun setAdapterView(stringArray: Array<String>): ArrayAdapter<String> {
        return object : ArrayAdapter<String>(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item, stringArray
        ) {
            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                val view: TextView = super.getDropDownView(position, convertView, parent) as TextView
                if (position == 0)
                    view.setTextColor(Color.LTGRAY)
                else view.setTextColor(Color.BLACK)
                return view
            }

            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }
        }
    }

    private fun setSpinner() {
        val listener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) { }

            override fun onItemSelected( parent: AdapterView<*>?, view: View?, position: Int, id: Long ) {
                val list = setSubCategoryList(position)
                sub_category_edit.adapter = setAdapterView(list)
                sub_category_edit.performClick()
            }
        }
        category_edit.setOnTouchListener { _, _ -> category_edit.onItemSelectedListener = listener; false}
    }

    private fun setSubCategoryList(index: Int): Array<String> {
        var list = Array(0) { "" }
        when (index) {
            1 -> list = resources.getStringArray(R.array.sub_category_list0)
            2 -> list = resources.getStringArray(R.array.sub_category_list1)
            3 -> list = resources.getStringArray(R.array.sub_category_list2)
            4 -> list = resources.getStringArray(R.array.sub_category_list3)
            5 -> list = resources.getStringArray(R.array.sub_category_list4)
            6 -> list = resources.getStringArray(R.array.sub_category_list5)
            7 -> list = resources.getStringArray(R.array.sub_category_list6)
            8 -> list = resources.getStringArray(R.array.sub_category_list7)
        }
        return list
    }

    override fun onDestroy() {
        if (!isRotating && !wantToSave) {
            if (mCurrentPhotoPath != "")
                File(mCurrentPhotoPath).apply { if (exists()) delete() }
            vm.newImagePath = vm.getItemLD().value!!.localImagePath
        }
        (activity as MainActivity).hideKeyboard()
        super.onDestroy()
    }

    // Floating context menu actions
    private fun pickFromGallery() {
        if (ActivityCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                codeGalleryRequest
            )
        } else {
            //Create an Intent with action as ACTION_PICK
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            // Sets the type as image/*. This ensures only components of type image are selected
            intent.type = "image/*"
            //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
            val mimeTypes = arrayOf("image/jpeg", "image/png")
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
            // Launching the Intent
            startActivityForResult(intent, codeGalleryRequest)
        }
    }

    // creating intent for camera
    private fun takePhoto() {
        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                codePhotoRequest
            )
        } else {
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                takePictureIntent.resolveActivity(requireActivity().packageManager)?.also {
                    // Create the File where the photo should go
                    try {
                        createImageFile().also {
                            val photoURI = FileProvider.getUriForFile(
                                requireContext(),
                                "com.example.amadzon2.fileprovider",
                                it
                            )
                            backupImgUri = uriString
                            uriString = photoURI.toString()

                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                            startActivityForResult(takePictureIntent, codePhotoRequest)
                        }
                    } catch (ex: Exception) {
                        // Error occurred while creating the File
                        showMessage("Capture Image Bug: ${ex.message}")
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == codeGalleryRequest) {

            data?.let {
                val outputFile = createImageFile()
                val photoURI = FileProvider.getUriForFile(
                    requireContext(),
                    "com.example.amadzon2.fileprovider",
                    outputFile
                )
                backupImgUri = uriString
                uriString = photoURI.toString()
                vm.newImagePath = outputFile.absolutePath
                val inputStream: InputStream? =
                    requireActivity().contentResolver.openInputStream(data.data!!)
                val fileOutputStream = FileOutputStream(outputFile)
                copyStream(inputStream!!, fileOutputStream)
                fileOutputStream.close()
                inputStream.close()
                rotateCompressSet()
            }
        }
        if (resultCode == Activity.RESULT_OK && requestCode == codePhotoRequest) {
            rotateCompressSet()
        } else if (resultCode == Activity.RESULT_CANCELED && requestCode == codePhotoRequest) {
            // if user quit camera without saving, we want to restore previous image
            File(mCurrentPhotoPath).apply { if (exists()) delete() }
            mCurrentPhotoPath = backupImgPath
            uriString = backupImgUri
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timestamp = SimpleDateFormat("ddMMyyyy_HHmmss", Locale.getDefault()).format(Date())
        val imageFileName = "profile_pic_$timestamp"
        val storageDir = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
            imageFileName, /* prefix */
            ".jpg", /* suffix */
            storageDir    /* directory */
        )

        backupImgPath = mCurrentPhotoPath
        mCurrentPhotoPath = image.absolutePath
        vm.newImagePath = image.absolutePath
        return image
    }

    private fun rotateCompressSet() {
        var img = BitmapFactory.decodeFile(mCurrentPhotoPath)
        img = shrinkImageSize(img)
        getRightRotation(img, mCurrentPhotoPath)
            ?.apply {
                requireActivity().contentResolver.openOutputStream(Uri.parse(uriString))
                    ?.apply {
                        compress(Bitmap.CompressFormat.JPEG, 100, this)
                        flush()
                        close()
                    }
                product_image.setImageBitmap(this)
                if (backupImgPath != "")
                    File(backupImgPath).apply { if (exists()) delete() }
            } ?: product_image.setImageResource(R.drawable.user)
    }

    private fun shrinkImageSize(btm: Bitmap): Bitmap {
        val res: Bitmap
        if (btm.width >= btm.height)
            res = Bitmap.createBitmap(
                btm,
                btm.width / 2 - btm.height / 2,
                0,
                btm.height,
                btm.height
            )
        else
            res = Bitmap.createBitmap(
                btm,
                0,
                btm.height / 2 - btm.width / 2,
                btm.width,
                btm.width
            )
        val newSize = requireActivity().resources.getInteger(R.integer.image_size)
        return Bitmap.createScaledBitmap(res, newSize, newSize, false)
    }

    private fun getRightRotation(bitmap: Bitmap, photoPath: String): Bitmap? {
        val ei = ExifInterface(photoPath)
        val orientation: Int = ei.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        )

        val rotatedBitmap: Bitmap?
        rotatedBitmap = when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(bitmap, 90f)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(bitmap, 180f)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(bitmap, 270f)
            ExifInterface.ORIENTATION_NORMAL -> bitmap
            else -> bitmap
        }
        return rotatedBitmap
    }

    private fun rotateImage(source: Bitmap, angle: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
    }

    @Throws(IOException::class)
    fun copyStream(input: InputStream, output: OutputStream) {
        val buffer = ByteArray(1024)
        var bytesRead: Int
        while (input.read(buffer).also { bytesRead = it } != -1) {
            output.write(buffer, 0, bytesRead)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            codePhotoRequest ->
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    takePhoto()
            codeGalleryRequest ->
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    pickFromGallery()
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun isValidUri(uri: String): Boolean {
        return try {
            requireActivity().contentResolver.openInputStream(Uri.parse(uri))
            true
        } catch (e: FileNotFoundException) {
            false
        }
    }

    private fun setFilter(): InputFilter {
        return object : InputFilter {
            override fun filter(
                source: CharSequence?,
                start: Int,
                end: Int,
                dest: Spanned?,
                dstart: Int,
                dend: Int
            ): CharSequence? {
                val builder = StringBuilder(dest.toString())
                val regex = Regex("(([1-9])([0-9]{0,5})?)?(\\.[0-9]{0,2})?")
                if (source != null) {
                    builder.replace(dstart, dend, source.subSequence(start, end).toString())
                    if (!builder.toString().matches(regex)) {
                        return if (source.isEmpty() && dest != null)
                            dest.subSequence(dstart, dend)
                        else ""
                    }
                }
                return null
            }
        }
    }

    private fun errorMessageS(){
        showMessage("Error in the image update", 2)
    }

    private fun errorMessageDB() {
        showMessage("Error in the dtabase connection", 2)
    }

    private fun writeItem(item :Item) {
        if (newItem) {
            vm.writeItemToDatabase(item, ::navigate, ::errorMessageDB)
        }
        else {
            vm.updateItemToDatabase(item, ::navigate, ::errorMessageDB)
        }
    }

    fun navigate() {

        llProgressBar.visibility = View.GONE

        val bundle = bundleOf(
            "group06.lab3.PRODUCT_ID" to productId.toString(),
            "group06.lab3.OWNER" to owner,
            "group06.lab3.MYITEMS" to true,
            "group06.lab3.MODIFIED" to true,
            "group06.lab3.STATE" to VALID
        )
        if (newItem) showMessage("Product correctly inserted", 1)
        else showMessage("Product correctly modified", 1)
        wantToSave = true
        navController.navigate(
            R.id.action_itemEditFragment_to_itemDetailsFragment,
            bundle
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        edititem_map_view.onCreate(savedInstanceState)
        edititem_map_view.onResume()
        edititem_map_view.getMapAsync(this)
    }

    override fun onMapReady(map: GoogleMap?) {
        map?.let {
            googleMap = it
            it.setOnCameraMoveStartedListener {
                scroll_item_edit.requestDisallowInterceptTouchEvent(true)
            }


            if(location_edit_txt.text.toString() != "")
                displayLocation(refactorLocation(location_edit_txt.text.toString()))
        }
    }

    private fun refactorLocation(l: String) : String{
        val tmp = l.replace(" ", "%20")
        return tmp.replace("+", "%2B")
    }

    private fun displayLocation(pos: String) : Boolean{
        val geocoder = Geocoder(requireContext(), Locale.ENGLISH)
        var addresses: List<Address> = emptyList()
        try {
            addresses = geocoder.getFromLocationName(pos, 1)
        }
        catch (e: IOException) {
            Toast.makeText(requireContext(), "Location loading failed", Toast.LENGTH_LONG).show()
        }

        if(addresses.isNotEmpty()) {
            val longitude = addresses[0].longitude
            val latitude= addresses[0].latitude

            if(addresses[0].locality != null)
                location_edit_txt.text = Editable.Factory.getInstance().newEditable(addresses[0].locality)
            else
                location_edit_txt.text = Editable.Factory.getInstance().newEditable(addresses[0].featureName)

            val itemL = LatLng(latitude, longitude)

            googleMap.addMarker(
                MarkerOptions().position(itemL)
                    .title("Your Location")
                    .snippet("This is your location")
            )
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(itemL, 12.0f))
            return true
        } else {
            Snackbar.make(requireView(), "This location does not exist! Check it out", Snackbar.LENGTH_SHORT)
                .setBackgroundTint(Color.parseColor("#C0392B")).show()
            return false
        }

    }
}
