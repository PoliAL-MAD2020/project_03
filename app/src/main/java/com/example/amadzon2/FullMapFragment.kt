package com.example.amadzon2

import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import kotlinx.android.synthetic.main.fragment_map_full.*
import java.io.IOException
import java.util.*


class FullMapFragment() : Fragment(), OnMapReadyCallback{

    private lateinit var googleMap: GoogleMap
    private var itemLoc: String = ""
    private var userLoc: String = ""
    private lateinit var vmu : UserViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        vmu =  ViewModelProvider(requireActivity()).get(UserViewModel::class.java)

        itemLoc = requireArguments().getString("group06.lab3.ITEMLOC").toString()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        map_view.onCreate(savedInstanceState)
        map_view.onResume()
        map_view.getMapAsync(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val x = inflater.inflate(R.layout.fragment_map_full, container, false)

        vmu.getUserLocation().observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            userLoc = it
            if(this::googleMap.isInitialized) {
                displayDestinations(refactorLocation(itemLoc), refactorLocation(userLoc))
            }
        })
        return x
    }

    override fun onMapReady(map: GoogleMap?) {
        map?.let {
            googleMap = it
            displayDestinations(refactorLocation(itemLoc), refactorLocation(userLoc))
        }
    }

    private fun refactorLocation(l: String) : String{
        val tmp = l.replace(" ", "%20")
        return tmp.replace("+", "%2B")
    }

    override fun onResume() {
        super.onResume()
        if(this::googleMap.isInitialized)
            displayDestinations(itemLoc, userLoc)
    }

    private fun displayDestinations(p1: String, p2: String){
        if(p1 == "" || p2 == ""){
            Log.w("aMADzon", "Error in the string format: could not load locations")
        } else {
            val geocoder = Geocoder(requireContext(), Locale.ENGLISH)
            var addresses1: List<Address> = emptyList()
            try {
                addresses1 = geocoder.getFromLocationName(p1, 1)
            }
            catch (e: IOException) {
                Toast.makeText(requireContext(), "Item location loading failed", Toast.LENGTH_LONG).show()
            }
            var addresses2: List<Address> = emptyList()
            try {
                addresses2 = geocoder.getFromLocationName(p2, 1)
            }
            catch (e: IOException) {
                Toast.makeText(requireContext(), "User location loading failed", Toast.LENGTH_LONG).show()
            }

            var itemL:LatLng? = null
            var usrL :LatLng? = null

            if(addresses1.isNotEmpty()) {
                val longitude1 = addresses1[0].longitude
                val latitude1= addresses1[0].latitude
                itemL = LatLng(latitude1, longitude1)

                googleMap.addMarker(
                    MarkerOptions().position(itemL)
                        .title("Item Location")
                )
            }

            if(addresses2.isNotEmpty()) {
                val longitude2 = addresses2[0].longitude
                val latitude2 = addresses2[0].latitude
                usrL = LatLng(latitude2, longitude2)
                googleMap.addMarker(
                    MarkerOptions().position(usrL)
                        .title("User Location")
                )
            }

            if(usrL != null && itemL != null) {
                val builder = LatLngBounds.Builder()
                builder.include(itemL)
                builder.include(usrL)
                val bounds = builder.build()
                googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))

                googleMap.addPolyline(
                    PolylineOptions().add(itemL, usrL).width(5f).color(Color.parseColor("#C0392B")).geodesic(true)
                )
            } else {
                if (usrL != null) {
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(usrL, 12.0f))
                }
                if (itemL != null) {
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(itemL, 12.0f))
                }
            }
        }

    }
}