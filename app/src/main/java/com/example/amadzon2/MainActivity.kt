package com.example.amadzon2

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.core.view.isVisible
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import java.io.File
import java.io.FileNotFoundException


class MainActivity : AppCompatActivity() {
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var toggle: ActionBarDrawerToggle
    private var productID: Int = -1

    lateinit var vm :UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // this is not very useful, probably only in case of accessibility stuff
        toggle =
            ActionBarDrawerToggle(
                this, drawer_layout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close
            ).apply { isDrawerIndicatorEnabled = true }

        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        vm = ViewModelProvider(this).get(UserViewModel::class.java)

        loadID()
        setupNavigation()
        setupDrawer()
        updateDrawer()

    }

    private fun setupNavigation() {
        setSupportActionBar(toolbar)
        val navController = findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(
            setOf(R.id.onSaleListFragment, R.id.itemListFragment, R.id.showProfileFragment, R.id.itemsOfInterestListFragment, R.id.boughtItemsListFragment),
            drawer_layout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        nav_view.setupWithNavController(navController)

        logout_button.setOnClickListener {
            vm.reset()
            logout()
            navController.navigate(R.id.loginFragment, null, getNavOptions())
        }
    }

    private fun getNavOptions(): NavOptions? {
        return NavOptions.Builder()
            .setEnterAnim(R.anim.slide_in_left)
            .setExitAnim(R.anim.slide_out_right)
            .setPopEnterAnim(R.anim.slide_in_right)
            .setPopExitAnim(R.anim.slide_out_left)
            .setPopUpTo(R.id.itemListFragment, true)
            .build()
    }

    private fun setupDrawer() {
        val navigationView: NavigationView = findViewById(R.id.nav_view)
        val headerView: View = navigationView.getHeaderView(0)
        val layout: LinearLayout = headerView.findViewById(R.id.link_to_profile)

        layout.setOnClickListener {
            val nav = findNavController(R.id.nav_host_fragment)
            if (nav.currentDestination?.id != R.id.showProfileFragment)
                nav.navigate(R.id.showProfileFragment)
            drawer_layout.closeDrawer(GravityCompat.START)
        }
    }

    fun updateDrawer() {
        val navigationView: NavigationView = findViewById(R.id.nav_view)
        val headerView: View = navigationView.getHeaderView(0)
        val nickname: TextView = headerView.findViewById(R.id.nav_nickname)
        val fullname: TextView = headerView.findViewById(R.id.nav_fullName)
        val image: ImageView = headerView.findViewById(R.id.nav_image)

        vm.getUser().observe(this, Observer { res ->
            fullname.text = res.full_name
            nickname.text = res.nickname
            if(res.localImagePath != "")
                image.setImageURI(Uri.fromFile(File(res.localImagePath)))
            else
                image.setImageResource(R.drawable.user)
        })

    }

    fun hideDrawer() {
        setDrawerEnabled(false)
        toolbar?.isVisible = false
    }

    fun showDrawer() {
        setDrawerEnabled(true)
        toolbar.isVisible = true
    }

    // Method to enable or disable drawer menu (but not the toolbar)
    private fun setDrawerEnabled(enabled: Boolean) {
        val lockMode: Int = if (enabled) DrawerLayout.LOCK_MODE_UNLOCKED else DrawerLayout.LOCK_MODE_LOCKED_CLOSED
        drawer_layout?.let {
            it.setDrawerLockMode(lockMode)
            toggle.isDrawerIndicatorEnabled = enabled
        }
    }

    fun setArrowInToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(false)
    }

    // It is needed to support click on top left button and handle its action (arrow or hamburger icon)
    // and also to manage navigation when using drawer menu
    override fun onSupportNavigateUp(): Boolean {
        return findNavController(R.id.nav_host_fragment).navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    // back button used for close drawer
    override fun onBackPressed() {
        when {
            drawer_layout.isDrawerOpen(GravityCompat.START) ->
                drawer_layout.closeDrawer(GravityCompat.START)
            findNavController(R.id.nav_host_fragment).currentDestination?.id == R.id.userInfoFragment -> {
                logout(true)
                super.onBackPressed()
            }
            else ->
                super.onBackPressed()
        }
    }

    private fun logout(revokeAccess: Boolean = false) {
        // Firebase sign out
        FirebaseAuth.getInstance().signOut()
        // Google revoke access or sign out
        vm.reset()
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.web_client_id))
            .requestEmail()
            .build()
        GoogleSignIn.getClient(this, gso).apply { if(revokeAccess) revokeAccess() else signOut() }
    }

    fun hideKeyboard() {
        val inputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        // Check if no view has focus
        val currentFocusedView = currentFocus
        currentFocusedView?.let {
            inputMethodManager.hideSoftInputFromWindow(
                currentFocusedView.windowToken, InputMethodManager.HIDE_NOT_ALWAYS
            )
        }
    }

    fun getItemId(): Int {
        return productID
    }

    fun getNewItemId(): Int {
        return ++productID
    }

    fun setItemId(newId:Int){
        productID = newId
    }

    private fun loadID() {
        val pref = getSharedPreferences("items_data", Context.MODE_PRIVATE)
        val id = pref?.getString("group06.lab3.GLOBAL_ID", "-1")
        productID = id!!.toInt()
    }

    private fun isValidUri(uri: String): Boolean {
        return try {
            contentResolver.openInputStream(Uri.parse(uri))
            true
        } catch (e: FileNotFoundException) {
            false
        }
    }

}
