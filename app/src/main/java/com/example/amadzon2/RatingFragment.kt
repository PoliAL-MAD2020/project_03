package com.example.amadzon2

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_rating.*
import org.json.JSONException
import org.json.JSONObject

class RatingFragment : Fragment(), RatingBar.OnRatingBarChangeListener {
    private lateinit var navController: NavController
    lateinit var vm :ItemViewModel
    private val FCM_API = "https://fcm.googleapis.com/fcm/send"
    private val serverkey =
        "key=" + "AAAAcaJU9K8:APA91bGR_1DhWaQzLX75hUpdpjlqORLDk4mEeM7DviW9ZjOxetMv0brePRKFjOXpFUjjrzdyPKfKpBTfXAMhDRPanekQUVcrKOhZDb18SjYZMuCqJRa_vZ1iTMhlLjWJRlt7vIELtStB"
    private val contentType = "application/json"
    private val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm = ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_rating, container, false)
    }

    override fun onRatingChanged(ratingBar: RatingBar?, rating: Float, fromUser: Boolean) {
        ratingValue.text = rating.toString()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        ratingBar.onRatingBarChangeListener = this
        sendButton.setOnClickListener {
            llProgressBar.visibility = View.VISIBLE
            val rating = ratingBar.rating
            vm.evaluateSeller(rating, comment_txt.text.toString(), ::success, ::error)
            sendNotification(vm.getItemLD().value!!.user)
        }
    }

    private fun error(){
        llProgressBar.visibility = View.GONE
        Snackbar.make(requireView(), "Error", Snackbar.LENGTH_SHORT)
            .setBackgroundTint(Color.parseColor("#C0392B")).show()
    }

    private fun success() {
        llProgressBar.visibility = View.GONE
        Snackbar.make(requireView(), "Your rating has been sent", Snackbar.LENGTH_SHORT)
            .setBackgroundTint(Color.parseColor("#4CAF50")).show()
        navController.navigate(R.id.action_ratingFragment_to_boughtItemsListFragment)
    }

    private fun sendNotification(id: String) {
        val notification = JSONObject()
        val notificationBody = JSONObject()

        val topic = "/topics/topic_" + id
        try {
            notificationBody.put("type", "E")
            notificationBody.put("title", "aMADzon")
            notificationBody.put("message", "You received an evaluation from one of your buyers!")
            notificationBody.put("destID", id)
            notification.put("to", topic)
            notification.put("data", notificationBody)
        } catch (e: JSONException) {
            Log.e("aMADzon", e.message.toString())
        }

        val jsonObjectRequest = object : JsonObjectRequest(FCM_API, notification,
            Response.Listener<JSONObject> { response ->
                Log.i("aMADzon", "onResponse: $response")
            },
            Response.ErrorListener {
                Toast.makeText(context, "Request Error", Toast.LENGTH_LONG).show()
            }) {
            override fun getHeaders(): MutableMap<String, String> {
                val params = HashMap<String, String>()
                params["Authorization"] = serverkey
                params["Content-Type"] = contentType
                return params
            }
        }
        requestQueue.add(jsonObjectRequest)
    }
}