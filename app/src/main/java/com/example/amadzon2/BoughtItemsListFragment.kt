package com.example.amadzon2

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_item_list.*

class BoughtItemsListFragment : Fragment(){
    private var navController: NavController? = null
    private var adapter: ItemAdapter? = null
    private lateinit var itemList : ArrayList<Item>
    private lateinit var vm : UserViewModel
    private lateinit var vmi : ItemViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)
        vmi = ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)

        vm.initBuyList(vm.getCurrentUserID(), { Log.w("aMADzon", "Some Error occurred")})
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_item_list, container, false)
        itemList = ArrayList()

        (rootView.findViewById(R.id.itemList) as RecyclerView).also {
            adapter = setupAdapter(it, itemList)
        }
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        setHasOptionsMenu(true)
        fab.visibility = View.GONE

        val str = "You haven't bought an item yet"
        emptyListMessage.text = str

        vm.getBuyList().observe(viewLifecycleOwner, Observer{
                items ->
            progressbar_il.hide()
            itemList.removeAll(itemList)
            itemList.addAll(items)
            adapter!!.itemListFull.clear()
            adapter!!.itemListFull.addAll(items)
            adapter!!.notifyDataSetChanged()
            if(items.size != 0)
                hideMessage()
            else
                displayMessage()
        })

        adapter!!.onItemClick = { item ->
            val bundle = bundleOf(
                "group06.lab3.PRODUCT_ID" to item.id.toString(),
                "group06.lab3.MYITEMS" to false,
                "group06.lab3.OWNER" to item.user,
                "group06.lab3.MODIFIED" to false,
                "group06.lab3.STATE" to item.state,
                "group06.lab3.RATED" to item.rated
            )
            vmi.initItemLD(item)
            navController!!.navigate(R.id.action_boughtItemsListFragment_to_itemDetailsFragment, bundle)
        }
    }

    private fun setupAdapter(v: RecyclerView, a: ArrayList<Item>) : ItemAdapter {
        v.layoutManager = LinearLayoutManager(activity)
        val ad = ItemAdapter(a, false)
        v.adapter = ad
        return ad
    }

    private fun hideMessage() {
        emptyList.visibility = View.GONE
    }

    private fun displayMessage() {
        emptyList.visibility = View.VISIBLE
    }

}