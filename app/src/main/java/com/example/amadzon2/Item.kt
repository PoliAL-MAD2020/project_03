package com.example.amadzon2


import com.google.firebase.Timestamp
import java.util.*

const val VALID = 0
const val SOLD = 1
const val LOCKED = 2

data class Item(
    var id:Int = 0, var name:String = "", var price: Double = 0.0, var description:String = "", var category:String = "", var user:String ="",
    var category_index:Int = 0, var sub_category_index:Int = 0, var location:String = "",
    var date:Timestamp = Timestamp(Date()), var state: Int = VALID,
    var localImagePath :String = "", var buyerID :String = "", var rated :Boolean = false) {
}

