package com.example.amadzon2

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Matrix
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.activity.addCallback
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.exifinterface.media.ExifInterface
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.edit_profile.*
import java.io.*
import java.text.SimpleDateFormat
import java.util.*

open class EditProfileFragment : Fragment(), OnMapReadyCallback {

    private lateinit var googleMap: GoogleMap
    private lateinit var navController: NavController
    private val codeGalleryRequest = 100
    private val codePhotoRequest = 101
    private var actualImageUri = ""
    private var uriString: String = ""
    private var mCurrentPhotoPath: String = ""
    private var backupImgPath: String = ""
    private var backupImgUri: String = ""
    private var isRotating = false
    private var wantToSave = false

    lateinit var vm: UserViewModel
    lateinit var storage: StorageReference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            findNavController().navigate(R.id.showProfileFragment, null, getNavOptions())
        }

        vm = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)
        storage = FirebaseStorage.getInstance().reference
    }

    private fun getNavOptions(): NavOptions? {
        return NavOptions.Builder()
            .setEnterAnim(R.anim.slide_in_left)
            .setExitAnim(R.anim.slide_out_right)
            .setPopEnterAnim(R.anim.slide_in_right)
            .setPopExitAnim(R.anim.slide_out_left)
            .setPopUpTo(R.id.mobile_navigation, true)
            .build()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.edit_profile, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        editprofile_map_view.onCreate(savedInstanceState)
        editprofile_map_view.onResume()
        editprofile_map_view.getMapAsync(this)
    }

    override fun onMapReady(map: GoogleMap?) {
        map?.let {
            googleMap = it
            it.setOnCameraMoveStartedListener {
                scroll_profile.requestDisallowInterceptTouchEvent(true)
            }

            if(location_profile_edit_txt.text.toString() != "")
                displayLocation(refactorLocation(location_profile_edit_txt.text.toString()))
        }
    }

    private fun refactorLocation(l: String) : String{
        val tmp = l.replace(" ", "%20")
        return tmp.replace("+", "%2B")
    }

    private fun displayLocation(pos: String) : Boolean{
        val geocoder = Geocoder(requireContext(), Locale.ENGLISH)
        var addresses: List<Address> = emptyList()
        try {
            addresses = geocoder.getFromLocationName(pos, 1)
        }
        catch (e: IOException) {
            Toast.makeText(requireContext(), "Location loading failed", Toast.LENGTH_LONG).show()
        }

        if(addresses.isNotEmpty()) {
            val longitude1 = addresses[0].longitude
            val latitude1= addresses[0].latitude

            if(addresses[0].locality != null)
                location_profile_edit_txt.text = Editable.Factory.getInstance().newEditable(addresses[0].locality)
            else
                location_profile_edit_txt.text = Editable.Factory.getInstance().newEditable(addresses[0].featureName)

            val itemL = LatLng(latitude1, longitude1)

            googleMap.addMarker(
                MarkerOptions().position(itemL)
                    .title("Your Location")
                    .snippet("This is your location")
            )
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(itemL, 12.0f))
            return true
        } else {
            Snackbar.make(requireView(), "This location does not exist! Check it out", Snackbar.LENGTH_SHORT)
                .setBackgroundTint(Color.parseColor("#C0392B")).show()
            return false
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("ImageURI", uriString)
        outState.putString("absPath", mCurrentPhotoPath)
        outState.putString("prevPath", backupImgPath)
        outState.putString("prevUri", backupImgUri)
        outState.putString("actualURI", actualImageUri)
        isRotating = true

        super.onSaveInstanceState(outState)
    }


    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        isRotating = false

        if (savedInstanceState != null) {
            mCurrentPhotoPath = savedInstanceState.getString("absPath").toString()
            backupImgPath = savedInstanceState.getString("prevPath").toString()
            backupImgUri = savedInstanceState.getString("prevUri").toString()
            uriString = savedInstanceState.getString("ImageURI").toString()
            actualImageUri = savedInstanceState.getString("actualURI").toString()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        navController = Navigation.findNavController(view)

        fullname_edit_txt.setOnFocusChangeListener { _, _ -> fullname_edit.error = null }
        nickname_edit_txt.setOnFocusChangeListener { _, _ -> nickname_edit.error = null }
        email_edit_txt.setOnFocusChangeListener { _, _ -> email_edit.error = null }
        location_profile_edit_txt.setOnFocusChangeListener { _, _ -> location_profile_edit.error = null }

        vm.getUser().observe(viewLifecycleOwner, Observer<User> { res ->
            fullname_edit_txt.text = Editable.Factory.getInstance().newEditable(res.full_name)
            email_edit_txt.text = Editable.Factory.getInstance().newEditable(res.email)
            nickname_edit_txt.text = Editable.Factory.getInstance().newEditable(res.nickname)
            location_profile_edit_txt.text =
                Editable.Factory.getInstance().newEditable(res.location)
            if (res.newImagePath != "")
                profile_image.setImageURI(Uri.fromFile(File(res.newImagePath)))
        })

        registerForContextMenu(uploadButton)
        uploadButton.setOnClickListener {
            requireActivity().openContextMenu(it)
        }
        location_profile_edit_txt.setOnEditorActionListener { _, actionId, _ ->
            if(actionId == EditorInfo.IME_ACTION_SEARCH){
                googleMap.clear()
                displayLocation(refactorLocation(location_profile_edit_txt.text.toString()))
                (activity as MainActivity).hideKeyboard()
                true
            } else {
                false
            }
        }
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        val inflater: MenuInflater = requireActivity().menuInflater
        if (navController.currentDestination?.id == R.id.editProfileFragment) {
            inflater.inflate(R.menu.upload_photo_menu, menu)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.save_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun isNotEmpty(e: TextInputLayout, message: String): Boolean {
        if (e.editText?.text.toString() == "") {
            e.error = message
            return false
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.save -> {
                (activity as MainActivity).hideKeyboard()

                if (!isNotEmpty(fullname_edit, "Enter your full name") or
                    !isNotEmpty(nickname_edit, "Enter a nickname") or
                    !isNotEmpty(email_edit, "Enter a valid email") or
                    !isNotEmpty(location_profile_edit, "Enter a location")
                )
                    return true

                if(displayLocation(refactorLocation(location_profile_edit_txt.text.toString()))) {

                    llProgressBar.visibility = View.VISIBLE

                    val a = requireActivity() as MainActivity
                    vm.setUser(vm.getCurrentUserID(),
                        fullname_edit_txt.text.toString().trim(),
                        email_edit_txt.text.toString().trim(),
                        nickname_edit_txt.text.toString().trim(),
                        location_profile_edit_txt.text.toString().trim(),
                        a.getItemId(),
                        { navigateOk() },
                        { errorMessageDB() })

                    true
                } else
                    true
            }
            android.R.id.home -> {
                findNavController().navigate(R.id.showProfileFragment, null, getNavOptions())
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showMessage(s: String, option: Int = 0) {
        val c: String? = when (option) {
            1 -> "#4CAF50"
            2 -> "#C0392B"
            else -> null
        }
        if (c != null)
            Snackbar.make(requireView(), s, Snackbar.LENGTH_SHORT)
                .setBackgroundTint(Color.parseColor(c)).show()
        else
            Snackbar.make(requireView(), s, Snackbar.LENGTH_SHORT).show()

    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        if (requireActivity().packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
            return when (item.itemId) {
                R.id.gallery_menu -> {
                    pickFromGallery()
                    true
                }
                R.id.camera_menu -> {
                    takePhoto()
                    true
                }
                else -> super.onContextItemSelected(item)
            }
        } else {
            // camera not available
            pickFromGallery()
            return true
        }
    }

    private fun pickFromGallery() {
        if (ActivityCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                codeGalleryRequest
            )
        } else {
            //Create an Intent with action as ACTION_PICK
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            // Sets the type as image/*. This ensures only components of type image are selected
            intent.type = "image/*"
            //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
            val mimeTypes = arrayOf("image/jpeg", "image/png")
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
            // Launching the Intent
            startActivityForResult(intent, codeGalleryRequest)
        }
    }

    // creating intent for camera
    private fun takePhoto() {
        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                codePhotoRequest
            )
        } else {
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                takePictureIntent.resolveActivity(requireActivity().packageManager)?.also {
                    // Create the File where the photo should go
                    try {
                        createImageFile().also {
                            val photoURI = FileProvider.getUriForFile(
                                requireContext(),
                                "com.example.amadzon2.fileprovider",
                                it
                            )
                            backupImgUri = uriString
                            uriString = photoURI.toString()

                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                            startActivityForResult(takePictureIntent, codePhotoRequest)
                        }
                    } catch (ex: Exception) {
                        // Error occurred while creating the File
                        displayMessage("Capture Image Bug: ${ex.message}")
                    }
                }
            }
        }

        isRotating = false

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        isRotating = false
        if (resultCode == Activity.RESULT_OK && requestCode == codeGalleryRequest) {

            data?.let {
                val outputFile = createImageFile()
                val photoURI = FileProvider.getUriForFile(
                    requireContext(),
                    "com.example.amadzon2.fileprovider",
                    outputFile
                )
                backupImgUri = uriString
                uriString = photoURI.toString()
                vm.setNewPath(outputFile.absolutePath)
                val inputStream: InputStream? =
                    requireActivity().contentResolver.openInputStream(data.data!!)
                val fileOutputStream = FileOutputStream(outputFile)
                copyStream(inputStream!!, fileOutputStream)
                fileOutputStream.close()
                inputStream.close()
                rotateCompressSet()
            }
        }
        if (resultCode == Activity.RESULT_OK && requestCode == codePhotoRequest) {
            rotateCompressSet()
        } else if (resultCode == Activity.RESULT_CANCELED && requestCode == codePhotoRequest) {
            // if user quit camera without saving, we want to restore previous image
            File(mCurrentPhotoPath).apply { if (exists()) delete() }
            mCurrentPhotoPath = backupImgPath
            uriString = backupImgUri
        }
    }

    private fun displayMessage(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timestamp = SimpleDateFormat("ddMMyyyy_HHmmss", Locale.getDefault()).format(Date())
        val imageFileName = "profile_pic_$timestamp"
        val storageDir = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
            imageFileName, /* prefix */
            ".jpg", /* suffix */
            storageDir    /* directory */
        )

        backupImgPath = mCurrentPhotoPath
        mCurrentPhotoPath = image.absolutePath
        vm.setNewPath(image.absolutePath)
        return image
    }

    private fun rotateCompressSet() {
        var img = BitmapFactory.decodeFile(mCurrentPhotoPath)
        img = shrinkImageSize(img)
        getRightRotation(img, mCurrentPhotoPath)
            ?.apply {
                requireActivity().contentResolver.openOutputStream(Uri.parse(uriString))
                    ?.apply {
                        compress(Bitmap.CompressFormat.JPEG, 100, this)
                        flush()
                        close()
                    }
                profile_image.setImageBitmap(this)
                if (backupImgPath != "")
                    File(backupImgPath).apply { if (exists()) delete() }
            } ?: profile_image.setImageResource(R.drawable.user)
    }

    private fun shrinkImageSize(btm: Bitmap): Bitmap {
        val res: Bitmap
        if (btm.width >= btm.height)
            res = Bitmap.createBitmap(
                btm,
                btm.width / 2 - btm.height / 2,
                0,
                btm.height,
                btm.height
            )
        else
            res = Bitmap.createBitmap(
                btm,
                0,
                btm.height / 2 - btm.width / 2,
                btm.width,
                btm.width
            )
        val newSize = requireActivity().resources.getInteger(R.integer.image_size)
        return Bitmap.createScaledBitmap(res, newSize, newSize, false)
    }

    private fun getRightRotation(bitmap: Bitmap, photoPath: String): Bitmap? {
        val ei = ExifInterface(photoPath)
        val orientation: Int = ei.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        )

        val rotatedBitmap: Bitmap?
        rotatedBitmap = when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(bitmap, 90f)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(bitmap, 180f)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(bitmap, 270f)
            ExifInterface.ORIENTATION_NORMAL -> bitmap
            else -> bitmap
        }
        return rotatedBitmap
    }

    private fun rotateImage(source: Bitmap, angle: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
    }

    @Throws(IOException::class)
    fun copyStream(input: InputStream, output: OutputStream) {
        val buffer = ByteArray(1024)
        var bytesRead: Int
        while (input.read(buffer).also { bytesRead = it } != -1) {
            output.write(buffer, 0, bytesRead)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            codePhotoRequest ->
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    takePhoto()
            codeGalleryRequest ->
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    pickFromGallery()
        }
    }


    override fun onDestroy() {
        if (!isRotating && !wantToSave) {
            if (mCurrentPhotoPath != "")
                File(mCurrentPhotoPath).apply { if (exists()) delete() }
            vm.setNewPath(vm.getUser().value!!.localImagePath)
        }
        (activity as MainActivity).hideKeyboard()
        super.onDestroy()
    }

    private fun navigateOk() {
        if (uriString != actualImageUri) {
            uploadButton.visibility = (View.INVISIBLE)
            vm.updateImage(
                mCurrentPhotoPath,
                { imageUpdateSuccessful() },
                { errorMessageStorage() })
        } else navigate()
    }

    private fun imageUpdateSuccessful() {
        vm.successfulUpdateImage({ navigate() }, { errorMessageStorage() })
    }

    private fun navigate() {
        llProgressBar.visibility = View.GONE
        navController.navigate(R.id.action_editProfileFragment_to_showProfileFragment)
        showMessage("Profile correctly updated", 1)
    }

    private fun errorMessageDB() {
        llProgressBar.visibility = View.GONE
        showMessage("Error with the database connection", 2)
    }

    private fun errorMessageStorage() {
        llProgressBar.visibility = View.GONE
        showMessage("Error with the image saving", 2)
    }
}