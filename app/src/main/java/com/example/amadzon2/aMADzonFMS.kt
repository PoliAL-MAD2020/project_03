package com.example.amadzon2

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.os.bundleOf
import androidx.navigation.NavDeepLinkBuilder
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.util.*

class aMADzonFMS : FirebaseMessagingService() {

    override fun onNewToken(token: String) {
        Log.d("aMADzon", "The token refreshed: $token")
    }

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationID = Random().nextInt(3000)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            setupChannels(notificationManager)

        if (FirebaseAuth.getInstance().currentUser?.uid.toString() == p0.data.get("destID")) {
            if (p0.data.get("type") == "I") {           //Notification for interest
                val bundle = bundleOf(
                    "group06.lab3.PRODUCT_ID" to p0.data.get("idItem").toString(),
                    "group06.lab3.MYITEMS" to true,
                    "group06.lab3.OWNER" to p0.data.get("idUser"),
                    "group06.lab3.MODIFIED" to false,
                    "group06.lab3.NOTIFICATION" to true,
                    "group06.lab3.VALID" to VALID
                )
                val pendingIntent = NavDeepLinkBuilder(this)
                    .setComponentName(MainActivity::class.java)
                    .setGraph(R.navigation.mobile_navigation)
                    .setDestination(R.id.itemDetailsFragment)
                    .setArguments(bundle)
                    .createPendingIntent()
                val largeicon = BitmapFactory.decodeResource(resources, R.drawable.ic_fire)
                val sounduri = RingtoneManager.getDefaultUri((RingtoneManager.TYPE_NOTIFICATION))
                val builder = NotificationCompat.Builder(this, "admin_channel")
                    .setSmallIcon(R.drawable.ic_fire)
                    .setLargeIcon(largeicon)
                    .setContentTitle(p0.data.get("title"))
                    .setContentText(p0.data.get("message"))
                    .setAutoCancel(true)
                    .setSound(sounduri)
                    .setContentIntent(pendingIntent)
                notificationManager.notify(notificationID, builder.build())
            }

            if (p0.data.get("type") == "S") {            //Notification for sell
                val intent = Intent(this, MainActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                val pendingIntent =
                    PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
                val largeicon = BitmapFactory.decodeResource(resources, R.drawable.ic_sold)
                val sounduri = RingtoneManager.getDefaultUri((RingtoneManager.TYPE_NOTIFICATION))
                val builder = NotificationCompat.Builder(this, "admin_channel")
                    .setSmallIcon(R.drawable.ic_sold)
                    .setLargeIcon(largeicon)
                    .setContentTitle(p0.data.get("title"))
                    .setContentText(p0.data.get("message"))
                    .setAutoCancel(true)
                    .setSound(sounduri)
                    .setContentIntent(pendingIntent)
                notificationManager.notify(notificationID, builder.build())
            }

            if (p0.data.get("type") == "R") {            //Notification for removal
                val intent = Intent(this, MainActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                val pendingIntent =
                    PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
                val largeicon = BitmapFactory.decodeResource(resources, R.drawable.ic_ad_removed)
                val sounduri = RingtoneManager.getDefaultUri((RingtoneManager.TYPE_NOTIFICATION))
                val builder = NotificationCompat.Builder(this, "admin_channel")
                    .setSmallIcon(R.drawable.ic_ad_removed)
                    .setLargeIcon(largeicon)
                    .setContentTitle(p0.data.get("title"))
                    .setContentText(p0.data.get("message"))
                    .setAutoCancel(true)
                    .setSound(sounduri)
                    .setContentIntent(pendingIntent)
                notificationManager.notify(notificationID, builder.build())
            }

            if (p0.data.get("type") == "V") {            //Notification for the buyer
                val bundle = bundleOf(
                    "group06.lab3.PRODUCT_ID" to p0.data.get("productID"),
                    "group06.lab3.MYITEMS" to false,
                    "group06.lab3.OWNER" to p0.data.get("owner"),
                    "group06.lab3.MODIFIED" to false,
                    "group06.lab3.STATE" to SOLD,
                    "group06.lab3.NOTIFICATION" to true
                )
                val pendingIntent = NavDeepLinkBuilder(this)
                    .setComponentName(MainActivity::class.java)
                    .setGraph(R.navigation.mobile_navigation)
                    .setDestination(R.id.itemDetailsFragment)
                    .setArguments(bundle)
                    .createPendingIntent()
                val largeicon = BitmapFactory.decodeResource(resources, R.drawable.ic_buy)
                val sounduri = RingtoneManager.getDefaultUri((RingtoneManager.TYPE_NOTIFICATION))
                val builder = NotificationCompat.Builder(this, "admin_channel")
                    .setSmallIcon(R.drawable.ic_buy)
                    .setLargeIcon(largeicon)
                    .setContentTitle(p0.data.get("title"))
                    .setContentText(p0.data.get("message"))
                    .setAutoCancel(true)
                    .setSound(sounduri)
                    .setContentIntent(pendingIntent)
                notificationManager.notify(notificationID, builder.build())
            }
            if (p0.data.get("type") == "E") {           //Notification for evaluation
                val bundle = bundleOf(
                    "group06.lab4.NOTIFICATION" to true,
                    "group06.lab4.USERID" to p0.data.get("destID")
                )
                val pendingIntent = NavDeepLinkBuilder(this)
                    .setComponentName(MainActivity::class.java)
                    .setGraph(R.navigation.mobile_navigation)
                    .setDestination(R.id.evaluationsListFragment)
                    .setArguments(bundle)
                    .createPendingIntent()
                val largeicon = BitmapFactory.decodeResource(resources, R.drawable.ic_rating)
                val sounduri = RingtoneManager.getDefaultUri((RingtoneManager.TYPE_NOTIFICATION))
                val builder = NotificationCompat.Builder(this, "admin_channel")
                    .setSmallIcon(R.drawable.ic_rating)
                    .setLargeIcon(largeicon)
                    .setContentTitle(p0.data.get("title"))
                    .setContentText(p0.data.get("message"))
                    .setAutoCancel(true)
                    .setSound(sounduri)
                    .setContentIntent(pendingIntent)
                notificationManager.notify(notificationID, builder.build())
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun setupChannels(notificationManager: NotificationManager?) {
        val adminChannelName = "New notification"
        val adminChannelDescription = "Device to device notification"

        val adminChannel: NotificationChannel
        adminChannel = NotificationChannel("admin_channel", adminChannelName, NotificationManager.IMPORTANCE_HIGH)
        adminChannel.description = adminChannelDescription
        adminChannel.enableLights(true)
        adminChannel.lightColor = Color.YELLOW
        adminChannel.enableVibration(true)
        notificationManager?.createNotificationChannel(adminChannel)
    }

}