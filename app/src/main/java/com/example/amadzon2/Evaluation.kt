package com.example.amadzon2

data class Evaluation (var sellerID :String, var buyerID :String, var buyerNickname :String, var rating :Float, var comment :String) {

    constructor() :this("","","", 0.0f, ""){
    }
}