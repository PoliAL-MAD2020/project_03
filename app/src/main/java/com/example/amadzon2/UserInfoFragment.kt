package com.example.amadzon2

import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.user_info.*
import java.io.IOException
import java.util.*

open class UserInfoFragment : Fragment(), OnMapReadyCallback {

    lateinit var vm :UserViewModel
    private lateinit var googleMap : GoogleMap
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        vm = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.user_info, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        info_map_view.onCreate(savedInstanceState)
        info_map_view.onResume()
        info_map_view.getMapAsync(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).hideDrawer()

        navController = Navigation.findNavController(view)
        signin_button.setOnClickListener {
            (activity as MainActivity).hideKeyboard()
            if ((!(!isNotEmpty(nickname_edit2, "Enter a nickname") or
                        !isNotEmpty(location_profile_edit2, "Enter a location"))
                        ) && displayLocation(refactorLocation(location_profile_edit_txt2.text.toString())))
            {
                lProgressBar.visibility = View.VISIBLE
                val a = requireActivity() as MainActivity
                vm.finalInit(vm.getCurrentUserID(),
                    nickname_edit_txt2.text.toString(),
                    location_profile_edit_txt2.text.toString(),
                    a.getItemId(),
                    { navigateRegisterEnd() },
                    { errorMessageDB() })
            }
        }

        location_profile_edit_txt2.setOnEditorActionListener { _, actionId, _ ->
            if(actionId == EditorInfo.IME_ACTION_SEARCH){
                if(this::googleMap.isInitialized) {
                    googleMap.clear()
                    displayLocation(refactorLocation(location_profile_edit_txt2.text.toString()))
                }
                (activity as MainActivity).hideKeyboard()
                true
            } else {
                false
            }
        }
    }

    override fun onMapReady(map: GoogleMap?) {
        map?.let {
            googleMap = it
            if(scroll_info != null) {
                it.setOnCameraMoveStartedListener {
                    scroll_info.requestDisallowInterceptTouchEvent(true)
                }
            }

            if(location_profile_edit_txt2.text.toString() != "")
                displayLocation(refactorLocation(location_profile_edit_txt2.text.toString()))
        }
    }

    private fun refactorLocation(l: String) : String{
        val tmp = l.replace(" ", "%20")
        return tmp.replace("+", "%2B")
    }

    private fun displayLocation(pos: String) : Boolean{
        val geocoder = Geocoder(requireContext(), Locale.ENGLISH)
        var addresses: List<Address> = emptyList()
        try {
            addresses = geocoder.getFromLocationName(pos, 1)
        }
        catch (e: IOException) {
            Toast.makeText(requireContext(), "Location loading failed", Toast.LENGTH_LONG).show()
            //TODO check this line
            //return false
        }

        if(addresses.isNotEmpty()) {
            val longitude1 = addresses[0].longitude
            val latitude1= addresses[0].latitude

            if(addresses[0].locality != null)
                location_profile_edit_txt2.text = Editable.Factory.getInstance().newEditable(addresses[0].locality)
            else
                location_profile_edit_txt2.text = Editable.Factory.getInstance().newEditable(addresses[0].featureName)

            val itemL = LatLng(latitude1, longitude1)

            googleMap.addMarker(
                MarkerOptions().position(itemL)
                    .title("Your Location")
                    .snippet("This is your location")
            )
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(itemL, 12.0f))
            return true
        } else {
            Snackbar.make(requireView(), "This Location does not exist! Check it out", Snackbar.LENGTH_SHORT)
                .setBackgroundTint(Color.parseColor("#C0392B")).show()
            return false
        }

    }
    private fun isNotEmpty(e: TextInputLayout, message: String): Boolean {
        if (e.editText?.text.toString() == "") {
            e.error = message
            return false
        }
        return true
    }

    private fun navigateRegisterEnd() {
        lProgressBar.visibility = View.GONE
        navController.navigate(R.id.action_userInfoFragment_to_onSaleListFragment)
        (activity as MainActivity).apply { showDrawer(); updateDrawer() }
    }

    private fun errorMessageDB() {
        lProgressBar.visibility = View.GONE
        Snackbar.make(requireView(), "Error with the database connection", Snackbar.LENGTH_LONG).show()
    }

}